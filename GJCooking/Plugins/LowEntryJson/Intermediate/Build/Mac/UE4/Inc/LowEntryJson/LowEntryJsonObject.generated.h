// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOWENTRYJSON_LowEntryJsonObject_generated_h
#error "LowEntryJsonObject.generated.h already included, missing '#pragma once' in LowEntryJsonObject.h"
#endif
#define LOWENTRYJSON_LowEntryJsonObject_generated_h

#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_RPC_WRAPPERS
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULowEntryJsonObject(); \
	friend LOWENTRYJSON_API class UClass* Z_Construct_UClass_ULowEntryJsonObject(); \
public: \
	DECLARE_CLASS(ULowEntryJsonObject, UObject, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonObject) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_INCLASS \
private: \
	static void StaticRegisterNativesULowEntryJsonObject(); \
	friend LOWENTRYJSON_API class UClass* Z_Construct_UClass_ULowEntryJsonObject(); \
public: \
	DECLARE_CLASS(ULowEntryJsonObject, UObject, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonObject) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonObject(ULowEntryJsonObject&&); \
	NO_API ULowEntryJsonObject(const ULowEntryJsonObject&); \
public:


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonObject(ULowEntryJsonObject&&); \
	NO_API ULowEntryJsonObject(const ULowEntryJsonObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonObject)


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_24_PROLOG
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_RPC_WRAPPERS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_INCLASS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LowEntryJsonObject."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
