// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOWENTRYJSONEDITOR_K2Node_LowEntry_CreateJsonArray_generated_h
#error "K2Node_LowEntry_CreateJsonArray.generated.h already included, missing '#pragma once' in K2Node_LowEntry_CreateJsonArray.h"
#endif
#define LOWENTRYJSONEDITOR_K2Node_LowEntry_CreateJsonArray_generated_h

#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_RPC_WRAPPERS
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_LowEntry_CreateJsonArray(); \
	friend LOWENTRYJSONEDITOR_API class UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArray(); \
public: \
	DECLARE_CLASS(UK2Node_LowEntry_CreateJsonArray, UK2Node, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/LowEntryJsonEditor"), LOWENTRYJSONEDITOR_API) \
	DECLARE_SERIALIZER(UK2Node_LowEntry_CreateJsonArray) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_LowEntry_CreateJsonArray(); \
	friend LOWENTRYJSONEDITOR_API class UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArray(); \
public: \
	DECLARE_CLASS(UK2Node_LowEntry_CreateJsonArray, UK2Node, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/LowEntryJsonEditor"), LOWENTRYJSONEDITOR_API) \
	DECLARE_SERIALIZER(UK2Node_LowEntry_CreateJsonArray) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonArray(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_LowEntry_CreateJsonArray) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LOWENTRYJSONEDITOR_API, UK2Node_LowEntry_CreateJsonArray); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_LowEntry_CreateJsonArray); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonArray(UK2Node_LowEntry_CreateJsonArray&&); \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonArray(const UK2Node_LowEntry_CreateJsonArray&); \
public:


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonArray(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonArray(UK2Node_LowEntry_CreateJsonArray&&); \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonArray(const UK2Node_LowEntry_CreateJsonArray&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LOWENTRYJSONEDITOR_API, UK2Node_LowEntry_CreateJsonArray); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_LowEntry_CreateJsonArray); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_LowEntry_CreateJsonArray)


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_19_PROLOG
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_RPC_WRAPPERS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_INCLASS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h_22_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class K2Node_LowEntry_CreateJsonArray."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonArray_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
