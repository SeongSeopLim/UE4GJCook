// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/Classes/K2Node_LowEntry_CreateJsonObjectPure.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_LowEntry_CreateJsonObjectPure() {}
// Cross Module References
	LOWENTRYJSONEDITOR_API UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonObjectPure_NoRegister();
	LOWENTRYJSONEDITOR_API UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonObjectPure();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_LowEntryJsonEditor();
// End Cross Module References
	void UK2Node_LowEntry_CreateJsonObjectPure::StaticRegisterNativesUK2Node_LowEntry_CreateJsonObjectPure()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonObjectPure_NoRegister()
	{
		return UK2Node_LowEntry_CreateJsonObjectPure::StaticClass();
	}
	UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonObjectPure()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_UK2Node,
				(UObject* (*)())Z_Construct_UPackage__Script_LowEntryJsonEditor,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Classes/K2Node_LowEntry_CreateJsonObjectPure.h" },
				{ "ModuleRelativePath", "Public/Classes/K2Node_LowEntry_CreateJsonObjectPure.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumInputs_MetaData[] = {
				{ "ModuleRelativePath", "Public/Classes/K2Node_LowEntry_CreateJsonObjectPure.h" },
				{ "ToolTip", "The number of input pins to generate for this node" },
			};
#endif
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumInputs = { UE4CodeGen_Private::EPropertyClass::Int, "NumInputs", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000000, 1, nullptr, STRUCT_OFFSET(UK2Node_LowEntry_CreateJsonObjectPure, NumInputs), METADATA_PARAMS(NewProp_NumInputs_MetaData, ARRAY_COUNT(NewProp_NumInputs_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_NumInputs,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<UK2Node_LowEntry_CreateJsonObjectPure>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&UK2Node_LowEntry_CreateJsonObjectPure::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00080080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_LowEntry_CreateJsonObjectPure, 3691491394);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_LowEntry_CreateJsonObjectPure(Z_Construct_UClass_UK2Node_LowEntry_CreateJsonObjectPure, &UK2Node_LowEntry_CreateJsonObjectPure::StaticClass, TEXT("/Script/LowEntryJsonEditor"), TEXT("UK2Node_LowEntry_CreateJsonObjectPure"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_LowEntry_CreateJsonObjectPure);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
