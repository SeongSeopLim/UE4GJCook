// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/Classes/LowEntryJsonDebugLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLowEntryJsonDebugLibrary() {}
// Cross Module References
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonDebugLibrary_NoRegister();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonDebugLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_LowEntryJson();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonArray_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonObject_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonValue_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped();
// End Cross Module References
	void ULowEntryJsonDebugLibrary::StaticRegisterNativesULowEntryJsonDebugLibrary()
	{
		UClass* Class = ULowEntryJsonDebugLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "PrintBoolean", &ULowEntryJsonDebugLibrary::execPrintBoolean },
			{ "PrintBooleanArray", &ULowEntryJsonDebugLibrary::execPrintBooleanArray },
			{ "PrintByte", &ULowEntryJsonDebugLibrary::execPrintByte },
			{ "PrintByteArray", &ULowEntryJsonDebugLibrary::execPrintByteArray },
			{ "PrintFloat", &ULowEntryJsonDebugLibrary::execPrintFloat },
			{ "PrintFloatArray", &ULowEntryJsonDebugLibrary::execPrintFloatArray },
			{ "PrintInteger", &ULowEntryJsonDebugLibrary::execPrintInteger },
			{ "PrintIntegerArray", &ULowEntryJsonDebugLibrary::execPrintIntegerArray },
			{ "PrintJsonArray", &ULowEntryJsonDebugLibrary::execPrintJsonArray },
			{ "PrintJsonArrayArray", &ULowEntryJsonDebugLibrary::execPrintJsonArrayArray },
			{ "PrintJsonObject", &ULowEntryJsonDebugLibrary::execPrintJsonObject },
			{ "PrintJsonObjectArray", &ULowEntryJsonDebugLibrary::execPrintJsonObjectArray },
			{ "PrintJsonValue", &ULowEntryJsonDebugLibrary::execPrintJsonValue },
			{ "PrintJsonValueArray", &ULowEntryJsonDebugLibrary::execPrintJsonValueArray },
			{ "PrintStringArrayEscaped", &ULowEntryJsonDebugLibrary::execPrintStringArrayEscaped },
			{ "PrintStringEscaped", &ULowEntryJsonDebugLibrary::execPrintStringEscaped },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean()
	{
		struct LowEntryJsonDebugLibrary_eventPrintBoolean_Parms
		{
			UObject* WorldContextObject;
			bool Boolean;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintBoolean_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintBoolean_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Boolean_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_Boolean_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintBoolean_Parms*)Obj)->Boolean = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Boolean = { UE4CodeGen_Private::EPropertyClass::Bool, "Boolean", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_Boolean_SetBit)>::SetBit, METADATA_PARAMS(NewProp_Boolean_MetaData, ARRAY_COUNT(NewProp_Boolean_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Boolean,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray()
	{
		struct LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms
		{
			UObject* WorldContextObject;
			TArray<bool> BooleanArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BooleanArray_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BooleanArray = { UE4CodeGen_Private::EPropertyClass::Array, "BooleanArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, BooleanArray), METADATA_PARAMS(NewProp_BooleanArray_MetaData, ARRAY_COUNT(NewProp_BooleanArray_MetaData)) };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BooleanArray_Inner = { UE4CodeGen_Private::EPropertyClass::Bool, "BooleanArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_BooleanArray,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_BooleanArray_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintBooleanArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte()
	{
		struct LowEntryJsonDebugLibrary_eventPrintByte_Parms
		{
			UObject* WorldContextObject;
			uint8 Byte;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintByte_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintByte_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintByte_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintByte_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Byte_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FBytePropertyParams NewProp_Byte = { UE4CodeGen_Private::EPropertyClass::Byte, "Byte", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, Byte), nullptr, METADATA_PARAMS(NewProp_Byte_MetaData, ARRAY_COUNT(NewProp_Byte_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Byte,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintByte_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray()
	{
		struct LowEntryJsonDebugLibrary_eventPrintByteArray_Parms
		{
			UObject* WorldContextObject;
			TArray<uint8> ByteArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintByteArray_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintByteArray_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ByteArray_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ByteArray = { UE4CodeGen_Private::EPropertyClass::Array, "ByteArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, ByteArray), METADATA_PARAMS(NewProp_ByteArray_MetaData, ARRAY_COUNT(NewProp_ByteArray_MetaData)) };
			static const UE4CodeGen_Private::FBytePropertyParams NewProp_ByteArray_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "ByteArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ByteArray,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ByteArray_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintByteArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat()
	{
		struct LowEntryJsonDebugLibrary_eventPrintFloat_Parms
		{
			UObject* WorldContextObject;
			float Float;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintFloat_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintFloat_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintFloat_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintFloat_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float = { UE4CodeGen_Private::EPropertyClass::Float, "Float", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, Float), METADATA_PARAMS(NewProp_Float_MetaData, ARRAY_COUNT(NewProp_Float_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Float,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintFloat_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray()
	{
		struct LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms
		{
			UObject* WorldContextObject;
			TArray<float> FloatArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatArray_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FloatArray = { UE4CodeGen_Private::EPropertyClass::Array, "FloatArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, FloatArray), METADATA_PARAMS(NewProp_FloatArray_MetaData, ARRAY_COUNT(NewProp_FloatArray_MetaData)) };
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatArray_Inner = { UE4CodeGen_Private::EPropertyClass::Float, "FloatArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FloatArray,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_FloatArray_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintFloatArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger()
	{
		struct LowEntryJsonDebugLibrary_eventPrintInteger_Parms
		{
			UObject* WorldContextObject;
			int32 Integer;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintInteger_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintInteger_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintInteger_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintInteger_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Integer_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_Integer = { UE4CodeGen_Private::EPropertyClass::Int, "Integer", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, Integer), METADATA_PARAMS(NewProp_Integer_MetaData, ARRAY_COUNT(NewProp_Integer_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Integer,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintInteger_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray()
	{
		struct LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms
		{
			UObject* WorldContextObject;
			TArray<int32> IntegerArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntegerArray_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IntegerArray = { UE4CodeGen_Private::EPropertyClass::Array, "IntegerArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, IntegerArray), METADATA_PARAMS(NewProp_IntegerArray_MetaData, ARRAY_COUNT(NewProp_IntegerArray_MetaData)) };
			static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegerArray_Inner = { UE4CodeGen_Private::EPropertyClass::Int, "IntegerArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_IntegerArray,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_IntegerArray_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintIntegerArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray()
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms
		{
			UObject* WorldContextObject;
			ULowEntryJsonArray* JsonArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_JsonArray,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray()
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms
		{
			UObject* WorldContextObject;
			TArray<ULowEntryJsonArray*> JsonArrayArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonArrayArray_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_JsonArrayArray = { UE4CodeGen_Private::EPropertyClass::Array, "JsonArrayArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, JsonArrayArray), METADATA_PARAMS(NewProp_JsonArrayArray_MetaData, ARRAY_COUNT(NewProp_JsonArrayArray_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArrayArray_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArrayArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_JsonArrayArray,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_JsonArrayArray_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonArrayArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject()
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms
		{
			UObject* WorldContextObject;
			ULowEntryJsonObject* JsonObject;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_JsonObject,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray()
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms
		{
			UObject* WorldContextObject;
			TArray<ULowEntryJsonObject*> JsonObjectArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonObjectArray_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_JsonObjectArray = { UE4CodeGen_Private::EPropertyClass::Array, "JsonObjectArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, JsonObjectArray), METADATA_PARAMS(NewProp_JsonObjectArray_MetaData, ARRAY_COUNT(NewProp_JsonObjectArray_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObjectArray_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObjectArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_JsonObjectArray,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_JsonObjectArray_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonObjectArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue()
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms
		{
			UObject* WorldContextObject;
			ULowEntryJsonValue* JsonValue;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_JsonValue,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonValue", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray()
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms
		{
			UObject* WorldContextObject;
			TArray<ULowEntryJsonValue*> JsonValueArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonValueArray_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_JsonValueArray = { UE4CodeGen_Private::EPropertyClass::Array, "JsonValueArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, JsonValueArray), METADATA_PARAMS(NewProp_JsonValueArray_MetaData, ARRAY_COUNT(NewProp_JsonValueArray_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValueArray_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValueArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_JsonValueArray,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_JsonValueArray_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonValueArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped()
	{
		struct LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms
		{
			UObject* WorldContextObject;
			TArray<FString> StringArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringArray_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StringArray = { UE4CodeGen_Private::EPropertyClass::Array, "StringArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, StringArray), METADATA_PARAMS(NewProp_StringArray_MetaData, ARRAY_COUNT(NewProp_StringArray_MetaData)) };
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringArray_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "StringArray", RF_Public|RF_Transient|RF_MarkAsNative, 0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_StringArray,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_StringArray_Inner,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "DisplayName", "Print String Array (Escaped)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintStringArrayEscaped", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped()
	{
		struct LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms
		{
			UObject* WorldContextObject;
			FString String;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(NewProp_TextColor_MetaData, ARRAY_COUNT(NewProp_TextColor_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToLog_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms*)Obj)->bPrintToLog = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToLog_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToLog_MetaData, ARRAY_COUNT(NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			auto NewProp_bPrintToScreen_SetBit = [](void* Obj){ ((LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms*)Obj)->bPrintToScreen = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_bPrintToScreen_SetBit)>::SetBit, METADATA_PARAMS(NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, ScreenDurationTime), METADATA_PARAMS(NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, Label), METADATA_PARAMS(NewProp_Label_MetaData, ARRAY_COUNT(NewProp_Label_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_String_MetaData[] = {
				{ "NativeConst", "" },
			};
#endif
			static const UE4CodeGen_Private::FStrPropertyParams NewProp_String = { UE4CodeGen_Private::EPropertyClass::Str, "String", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, String), METADATA_PARAMS(NewProp_String_MetaData, ARRAY_COUNT(NewProp_String_MetaData)) };
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_TextColor,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToLog,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_bPrintToScreen,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScreenDurationTime,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Label,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_String,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WorldContextObject,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "AdvancedDisplay", "3" },
				{ "CallableWithoutWorldContext", "" },
				{ "Category", "Low Entry|Json|Debug|Print" },
				{ "CPP_Default_bPrintToLog", "true" },
				{ "CPP_Default_bPrintToScreen", "true" },
				{ "CPP_Default_ScreenDurationTime", "5.000000" },
				{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
				{ "DisplayName", "Print String (Escaped)" },
				{ "Keywords", "log print" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
				{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
				{ "WorldContext", "WorldContextObject" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintStringEscaped", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULowEntryJsonDebugLibrary_NoRegister()
	{
		return ULowEntryJsonDebugLibrary::StaticClass();
	}
	UClass* Z_Construct_UClass_ULowEntryJsonDebugLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
				(UObject* (*)())Z_Construct_UPackage__Script_LowEntryJson,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean, "PrintBoolean" }, // 3622409516
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray, "PrintBooleanArray" }, // 2220535932
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte, "PrintByte" }, // 3559909917
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray, "PrintByteArray" }, // 170873012
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat, "PrintFloat" }, // 1604296406
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray, "PrintFloatArray" }, // 647170006
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger, "PrintInteger" }, // 391724017
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray, "PrintIntegerArray" }, // 1398590606
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray, "PrintJsonArray" }, // 2445929182
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray, "PrintJsonArrayArray" }, // 741427229
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject, "PrintJsonObject" }, // 2929594745
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray, "PrintJsonObjectArray" }, // 3737349116
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue, "PrintJsonValue" }, // 3390273456
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray, "PrintJsonValueArray" }, // 3448677711
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped, "PrintStringArrayEscaped" }, // 2226005858
				{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped, "PrintStringEscaped" }, // 3540996903
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Classes/LowEntryJsonDebugLibrary.h" },
				{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ULowEntryJsonDebugLibrary>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ULowEntryJsonDebugLibrary::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00100080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULowEntryJsonDebugLibrary, 3883585370);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULowEntryJsonDebugLibrary(Z_Construct_UClass_ULowEntryJsonDebugLibrary, &ULowEntryJsonDebugLibrary::StaticClass, TEXT("/Script/LowEntryJson"), TEXT("ULowEntryJsonDebugLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULowEntryJsonDebugLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
