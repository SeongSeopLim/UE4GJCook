// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOWENTRYJSON_LowEntryJsonArray_generated_h
#error "LowEntryJsonArray.generated.h already included, missing '#pragma once' in LowEntryJsonArray.h"
#endif
#define LOWENTRYJSON_LowEntryJsonArray_generated_h

#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_RPC_WRAPPERS
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULowEntryJsonArray(); \
	friend LOWENTRYJSON_API class UClass* Z_Construct_UClass_ULowEntryJsonArray(); \
public: \
	DECLARE_CLASS(ULowEntryJsonArray, UObject, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonArray) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_INCLASS \
private: \
	static void StaticRegisterNativesULowEntryJsonArray(); \
	friend LOWENTRYJSON_API class UClass* Z_Construct_UClass_ULowEntryJsonArray(); \
public: \
	DECLARE_CLASS(ULowEntryJsonArray, UObject, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonArray) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonArray(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonArray) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonArray); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonArray); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonArray(ULowEntryJsonArray&&); \
	NO_API ULowEntryJsonArray(const ULowEntryJsonArray&); \
public:


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonArray(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonArray(ULowEntryJsonArray&&); \
	NO_API ULowEntryJsonArray(const ULowEntryJsonArray&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonArray); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonArray); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonArray)


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_PRIVATE_PROPERTY_OFFSET
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_23_PROLOG
#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_RPC_WRAPPERS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_INCLASS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_PRIVATE_PROPERTY_OFFSET \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_INCLASS_NO_PURE_DECLS \
	HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LowEntryJsonArray."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
