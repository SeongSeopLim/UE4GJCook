// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/Enums/ELowEntryJsonValueAndTypeFound.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeELowEntryJsonValueAndTypeFound() {}
// Cross Module References
	LOWENTRYJSON_API UEnum* Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound();
	UPackage* Z_Construct_UPackage__Script_LowEntryJson();
// End Cross Module References
	static UEnum* ELowEntryJsonValueAndTypeFound_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, Z_Construct_UPackage__Script_LowEntryJson(), TEXT("ELowEntryJsonValueAndTypeFound"));
		}
		return Singleton;
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ELowEntryJsonValueAndTypeFound(ELowEntryJsonValueAndTypeFound_StaticEnum, TEXT("/Script/LowEntryJson"), TEXT("ELowEntryJsonValueAndTypeFound"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound_CRC() { return 3375789266U; }
	UEnum* Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LowEntryJson();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ELowEntryJsonValueAndTypeFound"), 0, Get_Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound_CRC(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ELowEntryJsonValueAndTypeFound::Success", (int64)ELowEntryJsonValueAndTypeFound::Success },
				{ "ELowEntryJsonValueAndTypeFound::NotFound", (int64)ELowEntryJsonValueAndTypeFound::NotFound },
				{ "ELowEntryJsonValueAndTypeFound::WrongType", (int64)ELowEntryJsonValueAndTypeFound::WrongType },
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/Enums/ELowEntryJsonValueAndTypeFound.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LowEntryJson,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				"ELowEntryJsonValueAndTypeFound",
				RF_Public|RF_Transient|RF_MarkAsNative,
				nullptr,
				(uint8)UEnum::ECppForm::EnumClass,
				"ELowEntryJsonValueAndTypeFound",
				Enumerators,
				ARRAY_COUNT(Enumerators),
				METADATA_PARAMS(Enum_MetaDataParams, ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
