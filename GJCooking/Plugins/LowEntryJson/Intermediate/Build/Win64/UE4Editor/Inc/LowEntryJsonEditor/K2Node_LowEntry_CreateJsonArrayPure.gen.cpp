// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LowEntryJsonEditor/Public/Classes/K2Node_LowEntry_CreateJsonArrayPure.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_LowEntry_CreateJsonArrayPure() {}
// Cross Module References
	LOWENTRYJSONEDITOR_API UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_NoRegister();
	LOWENTRYJSONEDITOR_API UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_LowEntryJsonEditor();
// End Cross Module References
	void UK2Node_LowEntry_CreateJsonArrayPure::StaticRegisterNativesUK2Node_LowEntry_CreateJsonArrayPure()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_NoRegister()
	{
		return UK2Node_LowEntry_CreateJsonArrayPure::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumInputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumInputs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_LowEntryJsonEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Classes/K2Node_LowEntry_CreateJsonArrayPure.h" },
		{ "ModuleRelativePath", "Public/Classes/K2Node_LowEntry_CreateJsonArrayPure.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::NewProp_NumInputs_MetaData[] = {
		{ "ModuleRelativePath", "Public/Classes/K2Node_LowEntry_CreateJsonArrayPure.h" },
		{ "ToolTip", "The number of input pins to generate for this node" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::NewProp_NumInputs = { UE4CodeGen_Private::EPropertyClass::Int, "NumInputs", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000000, 1, nullptr, STRUCT_OFFSET(UK2Node_LowEntry_CreateJsonArrayPure, NumInputs), METADATA_PARAMS(Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::NewProp_NumInputs_MetaData, ARRAY_COUNT(Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::NewProp_NumInputs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::NewProp_NumInputs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_LowEntry_CreateJsonArrayPure>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::ClassParams = {
		&UK2Node_LowEntry_CreateJsonArrayPure::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x000800A0u,
		nullptr, 0,
		Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_LowEntry_CreateJsonArrayPure, 2485063032);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_LowEntry_CreateJsonArrayPure(Z_Construct_UClass_UK2Node_LowEntry_CreateJsonArrayPure, &UK2Node_LowEntry_CreateJsonArrayPure::StaticClass, TEXT("/Script/LowEntryJsonEditor"), TEXT("UK2Node_LowEntry_CreateJsonArrayPure"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_LowEntry_CreateJsonArrayPure);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
