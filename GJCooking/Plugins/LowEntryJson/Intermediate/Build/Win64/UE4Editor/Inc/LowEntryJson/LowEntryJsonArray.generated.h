// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOWENTRYJSON_LowEntryJsonArray_generated_h
#error "LowEntryJsonArray.generated.h already included, missing '#pragma once' in LowEntryJsonArray.h"
#endif
#define LOWENTRYJSON_LowEntryJsonArray_generated_h

#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_RPC_WRAPPERS
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULowEntryJsonArray(); \
	friend struct Z_Construct_UClass_ULowEntryJsonArray_Statics; \
public: \
	DECLARE_CLASS(ULowEntryJsonArray, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonArray)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_INCLASS \
private: \
	static void StaticRegisterNativesULowEntryJsonArray(); \
	friend struct Z_Construct_UClass_ULowEntryJsonArray_Statics; \
public: \
	DECLARE_CLASS(ULowEntryJsonArray, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonArray)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonArray(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonArray) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonArray); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonArray); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonArray(ULowEntryJsonArray&&); \
	NO_API ULowEntryJsonArray(const ULowEntryJsonArray&); \
public:


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonArray(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonArray(ULowEntryJsonArray&&); \
	NO_API ULowEntryJsonArray(const ULowEntryJsonArray&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonArray); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonArray); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonArray)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_PRIVATE_PROPERTY_OFFSET
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_23_PROLOG
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_RPC_WRAPPERS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_INCLASS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_INCLASS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h_26_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LowEntryJsonArray."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonArray_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
