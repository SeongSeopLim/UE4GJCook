// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOWENTRYJSON_ELowEntryJsonValueFound_generated_h
#error "ELowEntryJsonValueFound.generated.h already included, missing '#pragma once' in ELowEntryJsonValueFound.h"
#endif
#define LOWENTRYJSON_ELowEntryJsonValueFound_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Enums_ELowEntryJsonValueFound_h


#define FOREACH_ENUM_ELOWENTRYJSONVALUEFOUND(op) \
	op(ELowEntryJsonValueFound::Found) \
	op(ELowEntryJsonValueFound::NotFound) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
