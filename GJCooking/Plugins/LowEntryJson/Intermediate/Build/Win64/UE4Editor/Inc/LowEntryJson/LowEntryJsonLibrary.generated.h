// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ULowEntryJsonValue;
class ULowEntryJsonArray;
class ULowEntryJsonObject;
enum class ELowEntryJsonTypeFound : uint8;
enum class ELowEntryJsonType : uint8;
enum class ELowEntryJsonValueFound : uint8;
enum class ELowEntryJsonValueAndTypeFound : uint8;
class ULowEntryJsonObjectIterator;
class ULowEntryJsonObjectEntry;
enum class ELowEntryJsonParseResult : uint8;
#ifdef LOWENTRYJSON_LowEntryJsonLibrary_generated_h
#error "LowEntryJsonLibrary.generated.h already included, missing '#pragma once' in LowEntryJsonLibrary.h"
#endif
#define LOWENTRYJSON_LowEntryJsonLibrary_generated_h

#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execValue_GetAsJsonArrayOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsJsonArrayOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsJsonObjectOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsJsonObjectOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsBooleanOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_UBOOL(Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsBooleanOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsByteOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsByteOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsFloatOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsFloatOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsIntegerOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsIntegerOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsStringOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsStringOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Value_GetJsonArray(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Value_GetJsonObject(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetBoolean(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetByte(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetFloat(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetInteger(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetString(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_GetNull(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Get) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonType,Z_Param_Out_Branch); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_ReturnStringValue); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ReturnIntegerValue); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_ReturnFloatValue); \
		P_GET_PROPERTY_REF(UByteProperty,Z_Param_Out_ReturnByteValue); \
		P_GET_UBOOL_REF(Z_Param_Out_ReturnBooleanValue); \
		P_GET_OBJECT_REF(ULowEntryJsonObject,Z_Param_Out_ReturnJsonObjectValue); \
		P_GET_OBJECT_REF(ULowEntryJsonArray,Z_Param_Out_ReturnJsonArrayValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_Get(Z_Param_JsonValue,(ELowEntryJsonType&)(Z_Param_Out_Branch),Z_Param_Out_ReturnStringValue,Z_Param_Out_ReturnIntegerValue,Z_Param_Out_ReturnFloatValue,Z_Param_Out_ReturnByteValue,Z_Param_Out_ReturnBooleanValue,Z_Param_Out_ReturnJsonObjectValue,Z_Param_Out_ReturnJsonArrayValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetJsonArray(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetJsonObject(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetBoolean(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetByte(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetFloat(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetInteger(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetString(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetNull(Z_Param_JsonValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Set) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_Set(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_Clone(Z_Param_JsonValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromJsonArray(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromJsonObject(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromBoolean) \
	{ \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromBoolean(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromByte) \
	{ \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromByte(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromFloat) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromFloat(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromInteger) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromInteger(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromString(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromNull) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromNull(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_Create) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_Create(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Clone(Z_Param_JsonValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromJsonArray(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromJsonObject(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromBoolean) \
	{ \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromBoolean(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromByte) \
	{ \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromByte(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromFloat) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromFloat(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromInteger) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromInteger(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromString(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromNull) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromNull(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Create) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Create(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Append) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_AppendJsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Append(Z_Param_JsonArray,Z_Param_AppendJsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Shorten) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Size); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Shorten(Z_Param_JsonArray,Z_Param_Size); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Expand) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Size); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Expand(Z_Param_JsonArray,Z_Param_Size); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Resize) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Size); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Resize(Z_Param_JsonArray,Z_Param_Size); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Length) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Array_Length(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsJsonValueOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsJsonValueOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsJsonArrayOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsJsonArrayOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsJsonObjectOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsJsonObjectOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsBooleanOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_UBOOL(Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsBooleanOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsByteOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsByteOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsFloatOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsFloatOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsIntegerOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsIntegerOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsStringOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsStringOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetJsonValue(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetJsonArray(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetJsonObject(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetBoolean(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetByte(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetFloat(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetInteger(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetString(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_GetNull(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Get) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonType,Z_Param_Out_Branch); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_ReturnStringValue); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ReturnIntegerValue); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_ReturnFloatValue); \
		P_GET_PROPERTY_REF(UByteProperty,Z_Param_Out_ReturnByteValue); \
		P_GET_UBOOL_REF(Z_Param_Out_ReturnBooleanValue); \
		P_GET_OBJECT_REF(ULowEntryJsonObject,Z_Param_Out_ReturnJsonObjectValue); \
		P_GET_OBJECT_REF(ULowEntryJsonArray,Z_Param_Out_ReturnJsonArrayValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Get(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonType&)(Z_Param_Out_Branch),Z_Param_Out_ReturnStringValue,Z_Param_Out_ReturnIntegerValue,Z_Param_Out_ReturnFloatValue,Z_Param_Out_ReturnByteValue,Z_Param_Out_ReturnBooleanValue,Z_Param_Out_ReturnJsonObjectValue,Z_Param_Out_ReturnJsonArrayValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertJsonValue(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertJsonArray(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertJsonObject(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertBoolean(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertByte(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertFloat(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertInteger(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertString(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertNull(Z_Param_JsonArray,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetJsonValue(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetJsonArray(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetJsonObject(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetBoolean(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetByte(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetFloat(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetInteger(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetString(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetNull(Z_Param_JsonArray,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddJsonValue(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddJsonArray(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddJsonObject(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddBoolean(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddByte(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddFloat(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddInteger(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddString(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddNull(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_IsSet) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Array_IsSet(Z_Param_JsonArray,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Unset) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Count); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Unset(Z_Param_JsonArray,Z_Param_Index,Z_Param_Count); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Clear) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Clear(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_ToJsonString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_UBOOL(Z_Param_PrettyPrint); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_ToJsonString(Z_Param_JsonArray,Z_Param_PrettyPrint); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_ToJsonString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_UBOOL(Z_Param_PrettyPrint); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Array_ToJsonString(Z_Param_JsonArray,Z_Param_PrettyPrint); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_Merge) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray1); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray2); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_Merge(Z_Param_JsonArray1,Z_Param_JsonArray2); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_Clone(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromJsonValueArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonValue*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromJsonValueArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromJsonArrayArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonArray*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromJsonArrayArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromJsonObjectArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonObject*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromJsonObjectArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromBooleanArray) \
	{ \
		P_GET_TARRAY_REF(bool,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromBooleanArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromByteArray) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromByteArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromFloatArray) \
	{ \
		P_GET_TARRAY_REF(float,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromFloatArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromIntegerArray) \
	{ \
		P_GET_TARRAY_REF(int32,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromIntegerArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromStringArray) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromStringArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Merge) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray1); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray2); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Merge(Z_Param_JsonArray1,Z_Param_JsonArray2); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Clone(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromJsonValueArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonValue*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromJsonValueArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromJsonArrayArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonArray*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromJsonArrayArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromJsonObjectArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonObject*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromJsonObjectArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromBooleanArray) \
	{ \
		P_GET_TARRAY_REF(bool,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromBooleanArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromByteArray) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromByteArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromFloatArray) \
	{ \
		P_GET_TARRAY_REF(float,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromFloatArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromIntegerArray) \
	{ \
		P_GET_TARRAY_REF(int32,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromIntegerArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromStringArray) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromStringArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_Length) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_Length(Z_Param_JsonObjectIterator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_GetValues) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_GetValues(Z_Param_JsonObjectIterator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_GetKeys) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_GetKeys(Z_Param_JsonObjectIterator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_Get) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_Key); \
		P_GET_OBJECT_REF(ULowEntryJsonValue,Z_Param_Out_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonValueFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::ObjectIterator_Get(Z_Param_JsonObjectIterator,Z_Param_Index,Z_Param_Out_Key,Z_Param_Out_JsonValue,(ELowEntryJsonValueFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectIterator**)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_Clone(Z_Param_JsonObjectIterator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_Create) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectIterator**)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_Create(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromJsonArray) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromJsonArray(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromJsonObject) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromJsonObject(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromBoolean) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromBoolean(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromByte) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromByte(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromFloat) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromFloat(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromInteger) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromInteger(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromString(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromNull) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromNull(Z_Param_Key); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_Create) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_Create(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Sort) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_UBOOL(Z_Param_Reversed); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Sort(Z_Param_JsonObject,Z_Param_Reversed); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Append) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_AppendJsonObject); \
		P_GET_UBOOL(Z_Param_OverrideDuplicates); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Append(Z_Param_JsonObject,Z_Param_AppendJsonObject,Z_Param_OverrideDuplicates); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Length) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Object_Length(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetValues) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetValues(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetKeys) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetKeys(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsJsonValueOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsJsonValueOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsJsonArrayOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsJsonArrayOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsJsonObjectOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsJsonObjectOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsBooleanOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_UBOOL(Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsBooleanOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsByteOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsByteOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsFloatOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsFloatOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsIntegerOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsIntegerOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsStringOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsStringOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetJsonValue(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetJsonArray(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetJsonObject(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetBoolean(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetByte(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetFloat(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetInteger(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetString(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_GetNull(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Get) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonType,Z_Param_Out_Branch); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_ReturnStringValue); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ReturnIntegerValue); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_ReturnFloatValue); \
		P_GET_PROPERTY_REF(UByteProperty,Z_Param_Out_ReturnByteValue); \
		P_GET_UBOOL_REF(Z_Param_Out_ReturnBooleanValue); \
		P_GET_OBJECT_REF(ULowEntryJsonObject,Z_Param_Out_ReturnJsonObjectValue); \
		P_GET_OBJECT_REF(ULowEntryJsonArray,Z_Param_Out_ReturnJsonArrayValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Get(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonType&)(Z_Param_Out_Branch),Z_Param_Out_ReturnStringValue,Z_Param_Out_ReturnIntegerValue,Z_Param_Out_ReturnFloatValue,Z_Param_Out_ReturnByteValue,Z_Param_Out_ReturnBooleanValue,Z_Param_Out_ReturnJsonObjectValue,Z_Param_Out_ReturnJsonArrayValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetJsonValue(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetJsonArray(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetJsonObject(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetBoolean(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetByte(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetFloat(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetInteger(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetString(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetNull(Z_Param_JsonObject,Z_Param_Key); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_IsSet) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Object_IsSet(Z_Param_JsonObject,Z_Param_Key); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Unset) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Unset(Z_Param_JsonObject,Z_Param_Key); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Clear) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Clear(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Pure_ToJsonString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_UBOOL(Z_Param_PrettyPrint); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Object_Pure_ToJsonString(Z_Param_JsonObject,Z_Param_PrettyPrint); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_ToJsonString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_UBOOL(Z_Param_PrettyPrint); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Object_ToJsonString(Z_Param_JsonObject,Z_Param_PrettyPrint); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Pure_Merge) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject1); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject2); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Pure_Merge(Z_Param_JsonObject1,Z_Param_JsonObject2); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Pure_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Pure_Clone(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Pure_CreateFromObjectEntryArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonObjectEntry*,Z_Param_Out_Array); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Pure_CreateFromObjectEntryArray(Z_Param_Out_Array); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Merge) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject1); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject2); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Merge(Z_Param_JsonObject1,Z_Param_JsonObject2); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Clone(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_CreateFromObjectEntryArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonObjectEntry*,Z_Param_Out_Array); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_CreateFromObjectEntryArray(Z_Param_Out_Array); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execJson_ParseJsonString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_JsonString); \
		P_GET_OBJECT_REF(ULowEntryJsonObject,Z_Param_Out_ReturnJsonObjectValue); \
		P_GET_OBJECT_REF(ULowEntryJsonArray,Z_Param_Out_ReturnJsonArrayValue); \
		P_GET_ENUM_REF(ELowEntryJsonParseResult,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Json_ParseJsonString(Z_Param_JsonString,Z_Param_Out_ReturnJsonObjectValue,Z_Param_Out_ReturnJsonArrayValue,(ELowEntryJsonParseResult&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReadEveryFiles) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=ULowEntryJsonLibrary::ReadEveryFiles(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execWrriteFile) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_filename); \
		P_GET_PROPERTY(UStrProperty,Z_Param_WrriteData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::WrriteFile(Z_Param_filename,Z_Param_WrriteData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReadFile2) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_filename); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::ReadFile2(Z_Param_filename); \
		P_NATIVE_END; \
	}


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execValue_GetAsJsonArrayOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsJsonArrayOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsJsonObjectOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsJsonObjectOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsBooleanOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_UBOOL(Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsBooleanOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsByteOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsByteOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsFloatOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsFloatOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsIntegerOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsIntegerOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetAsStringOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetAsStringOrDefault(Z_Param_JsonValue,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Value_GetJsonArray(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Value_GetJsonObject(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetBoolean(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetByte(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetFloat(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetInteger(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Value_GetString(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_GetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_GetNull(Z_Param_JsonValue,(ELowEntryJsonTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Get) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonType,Z_Param_Out_Branch); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_ReturnStringValue); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ReturnIntegerValue); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_ReturnFloatValue); \
		P_GET_PROPERTY_REF(UByteProperty,Z_Param_Out_ReturnByteValue); \
		P_GET_UBOOL_REF(Z_Param_Out_ReturnBooleanValue); \
		P_GET_OBJECT_REF(ULowEntryJsonObject,Z_Param_Out_ReturnJsonObjectValue); \
		P_GET_OBJECT_REF(ULowEntryJsonArray,Z_Param_Out_ReturnJsonArrayValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_Get(Z_Param_JsonValue,(ELowEntryJsonType&)(Z_Param_Out_Branch),Z_Param_Out_ReturnStringValue,Z_Param_Out_ReturnIntegerValue,Z_Param_Out_ReturnFloatValue,Z_Param_Out_ReturnByteValue,Z_Param_Out_ReturnBooleanValue,Z_Param_Out_ReturnJsonObjectValue,Z_Param_Out_ReturnJsonArrayValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetJsonArray(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetJsonObject(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetBoolean(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetByte(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetFloat(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetInteger(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetString(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_SetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_SetNull(Z_Param_JsonValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Set) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Value_Set(Z_Param_JsonValue,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_Clone(Z_Param_JsonValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromJsonArray(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromJsonObject(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromBoolean) \
	{ \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromBoolean(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromByte) \
	{ \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromByte(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromFloat) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromFloat(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromInteger) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromInteger(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromString(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_CreateFromNull) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_CreateFromNull(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Pure_Create) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Pure_Create(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Clone(Z_Param_JsonValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromJsonArray(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromJsonObject(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromBoolean) \
	{ \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromBoolean(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromByte) \
	{ \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromByte(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromFloat) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromFloat(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromInteger) \
	{ \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromInteger(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromString(Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_CreateFromNull) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_CreateFromNull(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execValue_Create) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Value_Create(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Append) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_AppendJsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Append(Z_Param_JsonArray,Z_Param_AppendJsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Shorten) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Size); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Shorten(Z_Param_JsonArray,Z_Param_Size); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Expand) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Size); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Expand(Z_Param_JsonArray,Z_Param_Size); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Resize) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Size); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Resize(Z_Param_JsonArray,Z_Param_Size); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Length) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Array_Length(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsJsonValueOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsJsonValueOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsJsonArrayOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsJsonArrayOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsJsonObjectOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsJsonObjectOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsBooleanOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_UBOOL(Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsBooleanOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsByteOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsByteOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsFloatOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsFloatOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsIntegerOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsIntegerOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetAsStringOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetAsStringOrDefault(Z_Param_JsonArray,Z_Param_Index,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetJsonValue(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetJsonArray(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Array_GetJsonObject(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetBoolean(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetByte(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetFloat(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetInteger(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Array_GetString(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_GetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_GetNull(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Get) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_ENUM_REF(ELowEntryJsonType,Z_Param_Out_Branch); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_ReturnStringValue); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ReturnIntegerValue); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_ReturnFloatValue); \
		P_GET_PROPERTY_REF(UByteProperty,Z_Param_Out_ReturnByteValue); \
		P_GET_UBOOL_REF(Z_Param_Out_ReturnBooleanValue); \
		P_GET_OBJECT_REF(ULowEntryJsonObject,Z_Param_Out_ReturnJsonObjectValue); \
		P_GET_OBJECT_REF(ULowEntryJsonArray,Z_Param_Out_ReturnJsonArrayValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Get(Z_Param_JsonArray,Z_Param_Index,(ELowEntryJsonType&)(Z_Param_Out_Branch),Z_Param_Out_ReturnStringValue,Z_Param_Out_ReturnIntegerValue,Z_Param_Out_ReturnFloatValue,Z_Param_Out_ReturnByteValue,Z_Param_Out_ReturnBooleanValue,Z_Param_Out_ReturnJsonObjectValue,Z_Param_Out_ReturnJsonArrayValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertJsonValue(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertJsonArray(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertJsonObject(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertBoolean(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertByte(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertFloat(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertInteger(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertString(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_InsertNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_InsertNull(Z_Param_JsonArray,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetJsonValue(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetJsonArray(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetJsonObject(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetBoolean(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetByte(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetFloat(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetInteger(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetString(Z_Param_JsonArray,Z_Param_Index,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_SetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_SetNull(Z_Param_JsonArray,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddJsonValue(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddJsonArray(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddJsonObject(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddBoolean(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddByte(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddFloat(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddInteger(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddString(Z_Param_JsonArray,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_AddNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_AddNull(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_IsSet) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Array_IsSet(Z_Param_JsonArray,Z_Param_Index); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Unset) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Count); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Unset(Z_Param_JsonArray,Z_Param_Index,Z_Param_Count); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Clear) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Array_Clear(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_ToJsonString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_UBOOL(Z_Param_PrettyPrint); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_ToJsonString(Z_Param_JsonArray,Z_Param_PrettyPrint); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_ToJsonString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_UBOOL(Z_Param_PrettyPrint); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Array_ToJsonString(Z_Param_JsonArray,Z_Param_PrettyPrint); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_Merge) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray1); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray2); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_Merge(Z_Param_JsonArray1,Z_Param_JsonArray2); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_Clone(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromJsonValueArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonValue*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromJsonValueArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromJsonArrayArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonArray*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromJsonArrayArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromJsonObjectArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonObject*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromJsonObjectArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromBooleanArray) \
	{ \
		P_GET_TARRAY_REF(bool,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromBooleanArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromByteArray) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromByteArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromFloatArray) \
	{ \
		P_GET_TARRAY_REF(float,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromFloatArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromIntegerArray) \
	{ \
		P_GET_TARRAY_REF(int32,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromIntegerArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Pure_CreateFromStringArray) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Pure_CreateFromStringArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Merge) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray1); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray2); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Merge(Z_Param_JsonArray1,Z_Param_JsonArray2); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_Clone(Z_Param_JsonArray); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromJsonValueArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonValue*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromJsonValueArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromJsonArrayArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonArray*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromJsonArrayArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromJsonObjectArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonObject*,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromJsonObjectArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromBooleanArray) \
	{ \
		P_GET_TARRAY_REF(bool,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromBooleanArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromByteArray) \
	{ \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromByteArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromFloatArray) \
	{ \
		P_GET_TARRAY_REF(float,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromFloatArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromIntegerArray) \
	{ \
		P_GET_TARRAY_REF(int32,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromIntegerArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execArray_CreateFromStringArray) \
	{ \
		P_GET_TARRAY_REF(FString,Z_Param_Out_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Array_CreateFromStringArray(Z_Param_Out_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_Length) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_Length(Z_Param_JsonObjectIterator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_GetValues) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_GetValues(Z_Param_JsonObjectIterator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_GetKeys) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_GetKeys(Z_Param_JsonObjectIterator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_Get) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Index); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_Key); \
		P_GET_OBJECT_REF(ULowEntryJsonValue,Z_Param_Out_JsonValue); \
		P_GET_ENUM_REF(ELowEntryJsonValueFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::ObjectIterator_Get(Z_Param_JsonObjectIterator,Z_Param_Index,Z_Param_Out_Key,Z_Param_Out_JsonValue,(ELowEntryJsonValueFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObjectIterator,Z_Param_JsonObjectIterator); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectIterator**)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_Clone(Z_Param_JsonObjectIterator); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectIterator_Create) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectIterator**)Z_Param__Result=ULowEntryJsonLibrary::ObjectIterator_Create(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromJsonArray) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromJsonArray(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromJsonObject) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromJsonObject(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromBoolean) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromBoolean(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromByte) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromByte(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromFloat) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromFloat(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromInteger) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromInteger(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromString(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_CreateFromNull) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_CreateFromNull(Z_Param_Key); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObjectEntry_Pure_Create) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObjectEntry**)Z_Param__Result=ULowEntryJsonLibrary::ObjectEntry_Pure_Create(Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Sort) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_UBOOL(Z_Param_Reversed); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Sort(Z_Param_JsonObject,Z_Param_Reversed); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Append) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_AppendJsonObject); \
		P_GET_UBOOL(Z_Param_OverrideDuplicates); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Append(Z_Param_JsonObject,Z_Param_AppendJsonObject,Z_Param_OverrideDuplicates); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Length) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Object_Length(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetValues) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetValues(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetKeys) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetKeys(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsJsonValueOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsJsonValueOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsJsonArrayOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsJsonArrayOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsJsonObjectOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsJsonObjectOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsBooleanOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_UBOOL(Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsBooleanOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsByteOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsByteOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsFloatOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsFloatOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsIntegerOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsIntegerOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetAsStringOrDefault) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Default); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetAsStringOrDefault(Z_Param_JsonObject,Z_Param_Key,Z_Param_Default); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonValue**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetJsonValue(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonArray**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetJsonArray(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_GetJsonObject(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetBoolean(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(uint8*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetByte(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(float*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetFloat(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(int32*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetInteger(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Object_GetString(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_GetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonValueAndTypeFound,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_GetNull(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonValueAndTypeFound&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Get) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_ENUM_REF(ELowEntryJsonType,Z_Param_Out_Branch); \
		P_GET_PROPERTY_REF(UStrProperty,Z_Param_Out_ReturnStringValue); \
		P_GET_PROPERTY_REF(UIntProperty,Z_Param_Out_ReturnIntegerValue); \
		P_GET_PROPERTY_REF(UFloatProperty,Z_Param_Out_ReturnFloatValue); \
		P_GET_PROPERTY_REF(UByteProperty,Z_Param_Out_ReturnByteValue); \
		P_GET_UBOOL_REF(Z_Param_Out_ReturnBooleanValue); \
		P_GET_OBJECT_REF(ULowEntryJsonObject,Z_Param_Out_ReturnJsonObjectValue); \
		P_GET_OBJECT_REF(ULowEntryJsonArray,Z_Param_Out_ReturnJsonArrayValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Get(Z_Param_JsonObject,Z_Param_Key,(ELowEntryJsonType&)(Z_Param_Out_Branch),Z_Param_Out_ReturnStringValue,Z_Param_Out_ReturnIntegerValue,Z_Param_Out_ReturnFloatValue,Z_Param_Out_ReturnByteValue,Z_Param_Out_ReturnBooleanValue,Z_Param_Out_ReturnJsonObjectValue,Z_Param_Out_ReturnJsonArrayValue); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetJsonValue) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetJsonValue(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetJsonArray) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetJsonArray(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetJsonObject) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetJsonObject(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetBoolean) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_UBOOL(Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetBoolean(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetByte) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetByte(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetFloat) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetFloat(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetInteger) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetInteger(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Value); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetString(Z_Param_JsonObject,Z_Param_Key,Z_Param_Value); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_SetNull) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_SetNull(Z_Param_JsonObject,Z_Param_Key); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_IsSet) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::Object_IsSet(Z_Param_JsonObject,Z_Param_Key); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Unset) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Key); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Unset(Z_Param_JsonObject,Z_Param_Key); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Clear) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Object_Clear(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Pure_ToJsonString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_UBOOL(Z_Param_PrettyPrint); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Object_Pure_ToJsonString(Z_Param_JsonObject,Z_Param_PrettyPrint); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_ToJsonString) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_UBOOL(Z_Param_PrettyPrint); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::Object_ToJsonString(Z_Param_JsonObject,Z_Param_PrettyPrint); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Pure_Merge) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject1); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject2); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Pure_Merge(Z_Param_JsonObject1,Z_Param_JsonObject2); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Pure_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Pure_Clone(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Pure_CreateFromObjectEntryArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonObjectEntry*,Z_Param_Out_Array); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Pure_CreateFromObjectEntryArray(Z_Param_Out_Array); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Merge) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject1); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject2); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Merge(Z_Param_JsonObject1,Z_Param_JsonObject2); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_Clone) \
	{ \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_Clone(Z_Param_JsonObject); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execObject_CreateFromObjectEntryArray) \
	{ \
		P_GET_TARRAY_REF(ULowEntryJsonObjectEntry*,Z_Param_Out_Array); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(ULowEntryJsonObject**)Z_Param__Result=ULowEntryJsonLibrary::Object_CreateFromObjectEntryArray(Z_Param_Out_Array); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execJson_ParseJsonString) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_JsonString); \
		P_GET_OBJECT_REF(ULowEntryJsonObject,Z_Param_Out_ReturnJsonObjectValue); \
		P_GET_OBJECT_REF(ULowEntryJsonArray,Z_Param_Out_ReturnJsonArrayValue); \
		P_GET_ENUM_REF(ELowEntryJsonParseResult,Z_Param_Out_Branch); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonLibrary::Json_ParseJsonString(Z_Param_JsonString,Z_Param_Out_ReturnJsonObjectValue,Z_Param_Out_ReturnJsonArrayValue,(ELowEntryJsonParseResult&)(Z_Param_Out_Branch)); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReadEveryFiles) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(TArray<FString>*)Z_Param__Result=ULowEntryJsonLibrary::ReadEveryFiles(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execWrriteFile) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_filename); \
		P_GET_PROPERTY(UStrProperty,Z_Param_WrriteData); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=ULowEntryJsonLibrary::WrriteFile(Z_Param_filename,Z_Param_WrriteData); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execReadFile2) \
	{ \
		P_GET_PROPERTY(UStrProperty,Z_Param_filename); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(FString*)Z_Param__Result=ULowEntryJsonLibrary::ReadFile2(Z_Param_filename); \
		P_NATIVE_END; \
	}


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULowEntryJsonLibrary(); \
	friend struct Z_Construct_UClass_ULowEntryJsonLibrary_Statics; \
public: \
	DECLARE_CLASS(ULowEntryJsonLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonLibrary)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_INCLASS \
private: \
	static void StaticRegisterNativesULowEntryJsonLibrary(); \
	friend struct Z_Construct_UClass_ULowEntryJsonLibrary_Statics; \
public: \
	DECLARE_CLASS(ULowEntryJsonLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonLibrary)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonLibrary(ULowEntryJsonLibrary&&); \
	NO_API ULowEntryJsonLibrary(const ULowEntryJsonLibrary&); \
public:


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonLibrary(ULowEntryJsonLibrary&&); \
	NO_API ULowEntryJsonLibrary(const ULowEntryJsonLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonLibrary)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_PRIVATE_PROPERTY_OFFSET
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_29_PROLOG
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_RPC_WRAPPERS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_INCLASS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_INCLASS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h_32_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LowEntryJsonLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
