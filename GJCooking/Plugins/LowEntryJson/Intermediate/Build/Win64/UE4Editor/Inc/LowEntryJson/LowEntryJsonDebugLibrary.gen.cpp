// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LowEntryJson/Public/Classes/LowEntryJsonDebugLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLowEntryJsonDebugLibrary() {}
// Cross Module References
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonDebugLibrary_NoRegister();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonDebugLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_LowEntryJson();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonArray_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonObject_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonValue_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped();
// End Cross Module References
	void ULowEntryJsonDebugLibrary::StaticRegisterNativesULowEntryJsonDebugLibrary()
	{
		UClass* Class = ULowEntryJsonDebugLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "PrintBoolean", &ULowEntryJsonDebugLibrary::execPrintBoolean },
			{ "PrintBooleanArray", &ULowEntryJsonDebugLibrary::execPrintBooleanArray },
			{ "PrintByte", &ULowEntryJsonDebugLibrary::execPrintByte },
			{ "PrintByteArray", &ULowEntryJsonDebugLibrary::execPrintByteArray },
			{ "PrintFloat", &ULowEntryJsonDebugLibrary::execPrintFloat },
			{ "PrintFloatArray", &ULowEntryJsonDebugLibrary::execPrintFloatArray },
			{ "PrintInteger", &ULowEntryJsonDebugLibrary::execPrintInteger },
			{ "PrintIntegerArray", &ULowEntryJsonDebugLibrary::execPrintIntegerArray },
			{ "PrintJsonArray", &ULowEntryJsonDebugLibrary::execPrintJsonArray },
			{ "PrintJsonArrayArray", &ULowEntryJsonDebugLibrary::execPrintJsonArrayArray },
			{ "PrintJsonObject", &ULowEntryJsonDebugLibrary::execPrintJsonObject },
			{ "PrintJsonObjectArray", &ULowEntryJsonDebugLibrary::execPrintJsonObjectArray },
			{ "PrintJsonValue", &ULowEntryJsonDebugLibrary::execPrintJsonValue },
			{ "PrintJsonValueArray", &ULowEntryJsonDebugLibrary::execPrintJsonValueArray },
			{ "PrintStringArrayEscaped", &ULowEntryJsonDebugLibrary::execPrintStringArrayEscaped },
			{ "PrintStringEscaped", &ULowEntryJsonDebugLibrary::execPrintStringEscaped },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintBoolean_Parms
		{
			UObject* WorldContextObject;
			bool Boolean;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Boolean_MetaData[];
#endif
		static void NewProp_Boolean_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Boolean;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintBoolean_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintBoolean_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Boolean_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Boolean_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintBoolean_Parms*)Obj)->Boolean = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Boolean = { UE4CodeGen_Private::EPropertyClass::Bool, "Boolean", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Boolean_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Boolean_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Boolean_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_Boolean,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms
		{
			UObject* WorldContextObject;
			TArray<bool> BooleanArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BooleanArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BooleanArray;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BooleanArray_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_BooleanArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_BooleanArray = { UE4CodeGen_Private::EPropertyClass::Array, "BooleanArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, BooleanArray), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_BooleanArray_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_BooleanArray_MetaData)) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_BooleanArray_Inner = { UE4CodeGen_Private::EPropertyClass::Bool, "BooleanArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_BooleanArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_BooleanArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintBooleanArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintBooleanArray_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintByte_Parms
		{
			UObject* WorldContextObject;
			uint8 Byte;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Byte_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Byte;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintByte_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintByte_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintByte_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintByte_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Byte_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Byte = { UE4CodeGen_Private::EPropertyClass::Byte, "Byte", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, Byte), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Byte_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Byte_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByte_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_Byte,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintByte_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintByteArray_Parms
		{
			UObject* WorldContextObject;
			TArray<uint8> ByteArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ByteArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ByteArray;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ByteArray_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintByteArray_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintByteArray_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ByteArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ByteArray = { UE4CodeGen_Private::EPropertyClass::Array, "ByteArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, ByteArray), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ByteArray_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ByteArray_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ByteArray_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "ByteArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ByteArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_ByteArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintByteArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintByteArray_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintFloat_Parms
		{
			UObject* WorldContextObject;
			float Float;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintFloat_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintFloat_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintFloat_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintFloat_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Float_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Float = { UE4CodeGen_Private::EPropertyClass::Float, "Float", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, Float), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Float_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Float_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloat_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_Float,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintFloat_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms
		{
			UObject* WorldContextObject;
			TArray<float> FloatArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FloatArray;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatArray_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_FloatArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_FloatArray = { UE4CodeGen_Private::EPropertyClass::Array, "FloatArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, FloatArray), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_FloatArray_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_FloatArray_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_FloatArray_Inner = { UE4CodeGen_Private::EPropertyClass::Float, "FloatArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_FloatArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_FloatArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintFloatArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintFloatArray_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintInteger_Parms
		{
			UObject* WorldContextObject;
			int32 Integer;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Integer_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Integer;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintInteger_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintInteger_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintInteger_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintInteger_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Integer_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Integer = { UE4CodeGen_Private::EPropertyClass::Int, "Integer", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, Integer), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Integer_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Integer_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintInteger_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_Integer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintInteger_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms
		{
			UObject* WorldContextObject;
			TArray<int32> IntegerArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntegerArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IntegerArray;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_IntegerArray_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_IntegerArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_IntegerArray = { UE4CodeGen_Private::EPropertyClass::Array, "IntegerArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, IntegerArray), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_IntegerArray_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_IntegerArray_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_IntegerArray_Inner = { UE4CodeGen_Private::EPropertyClass::Int, "IntegerArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_IntegerArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_IntegerArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintIntegerArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintIntegerArray_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms
		{
			UObject* WorldContextObject;
			ULowEntryJsonArray* JsonArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_Label_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_JsonArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms
		{
			UObject* WorldContextObject;
			TArray<ULowEntryJsonArray*> JsonArrayArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonArrayArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_JsonArrayArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArrayArray_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_JsonArrayArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_JsonArrayArray = { UE4CodeGen_Private::EPropertyClass::Array, "JsonArrayArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, JsonArrayArray), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_JsonArrayArray_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_JsonArrayArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_JsonArrayArray_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArrayArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_JsonArrayArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_JsonArrayArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonArrayArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonArrayArray_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms
		{
			UObject* WorldContextObject;
			ULowEntryJsonObject* JsonObject;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_Label_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_JsonObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms
		{
			UObject* WorldContextObject;
			TArray<ULowEntryJsonObject*> JsonObjectArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonObjectArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_JsonObjectArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObjectArray_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_JsonObjectArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_JsonObjectArray = { UE4CodeGen_Private::EPropertyClass::Array, "JsonObjectArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, JsonObjectArray), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_JsonObjectArray_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_JsonObjectArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_JsonObjectArray_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObjectArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_JsonObjectArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_JsonObjectArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonObjectArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonObjectArray_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms
		{
			UObject* WorldContextObject;
			ULowEntryJsonValue* JsonValue;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_Label_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_JsonValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonValue", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValue_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms
		{
			UObject* WorldContextObject;
			TArray<ULowEntryJsonValue*> JsonValueArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonValueArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_JsonValueArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValueArray_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_JsonValueArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_JsonValueArray = { UE4CodeGen_Private::EPropertyClass::Array, "JsonValueArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, JsonValueArray), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_JsonValueArray_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_JsonValueArray_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_JsonValueArray_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValueArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_JsonValueArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_JsonValueArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintJsonValueArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintJsonValueArray_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms
		{
			UObject* WorldContextObject;
			TArray<FString> StringArray;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StringArray;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringArray_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_StringArray_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_StringArray = { UE4CodeGen_Private::EPropertyClass::Array, "StringArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, StringArray), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_StringArray_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_StringArray_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_StringArray_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "StringArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_StringArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_StringArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "DisplayName", "Print String Array (Escaped)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintStringArrayEscaped", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04C22401, sizeof(LowEntryJsonDebugLibrary_eventPrintStringArrayEscaped_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics
	{
		struct LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms
		{
			UObject* WorldContextObject;
			FString String;
			FString Label;
			float ScreenDurationTime;
			bool bPrintToScreen;
			bool bPrintToLog;
			FLinearColor TextColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToLog_MetaData[];
#endif
		static void NewProp_bPrintToLog_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToLog;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrintToScreen_MetaData[];
#endif
		static void NewProp_bPrintToScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrintToScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenDurationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScreenDurationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Label_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Label;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_String_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_String;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_TextColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_TextColor = { UE4CodeGen_Private::EPropertyClass::Struct, "TextColor", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, TextColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_TextColor_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_TextColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToLog_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToLog_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms*)Obj)->bPrintToLog = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToLog = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToLog", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToLog_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToLog_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToLog_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToScreen_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToScreen_SetBit(void* Obj)
	{
		((LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms*)Obj)->bPrintToScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToScreen = { UE4CodeGen_Private::EPropertyClass::Bool, "bPrintToScreen", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms), &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToScreen_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToScreen_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_ScreenDurationTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_ScreenDurationTime = { UE4CodeGen_Private::EPropertyClass::Float, "ScreenDurationTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010040000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, ScreenDurationTime), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_ScreenDurationTime_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_ScreenDurationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_Label_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_Label = { UE4CodeGen_Private::EPropertyClass::Str, "Label", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, Label), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_Label_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_Label_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_String_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_String = { UE4CodeGen_Private::EPropertyClass::Str, "String", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, String), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_String_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_String_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_WorldContextObject = { UE4CodeGen_Private::EPropertyClass::Object, "WorldContextObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_TextColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToLog,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_bPrintToScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_ScreenDurationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_Label,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_String,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::NewProp_WorldContextObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "CallableWithoutWorldContext", "" },
		{ "Category", "Low Entry|Json|Debug|Print" },
		{ "CPP_Default_bPrintToLog", "true" },
		{ "CPP_Default_bPrintToScreen", "true" },
		{ "CPP_Default_ScreenDurationTime", "5.000000" },
		{ "CPP_Default_TextColor", "(R=0.000000,G=0.660000,B=1.000000,A=1.000000)" },
		{ "DisplayName", "Print String (Escaped)" },
		{ "Keywords", "log print" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
		{ "ToolTip", "Creates a Json Object, sets the given variable as a value, converts the created Json Object to a Json String, formats the Json String, and prints the formatted Json String using KismetSystemLibrary's PrintString." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonDebugLibrary, "PrintStringEscaped", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04822401, sizeof(LowEntryJsonDebugLibrary_eventPrintStringEscaped_Parms), Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULowEntryJsonDebugLibrary_NoRegister()
	{
		return ULowEntryJsonDebugLibrary::StaticClass();
	}
	struct Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LowEntryJson,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBoolean, "PrintBoolean" }, // 3020773605
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintBooleanArray, "PrintBooleanArray" }, // 3418225527
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByte, "PrintByte" }, // 2390241666
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintByteArray, "PrintByteArray" }, // 2740201727
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloat, "PrintFloat" }, // 1019423611
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintFloatArray, "PrintFloatArray" }, // 617667849
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintInteger, "PrintInteger" }, // 3109413314
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintIntegerArray, "PrintIntegerArray" }, // 745594128
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArray, "PrintJsonArray" }, // 3562325559
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonArrayArray, "PrintJsonArrayArray" }, // 3962472922
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObject, "PrintJsonObject" }, // 837767881
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonObjectArray, "PrintJsonObjectArray" }, // 69153868
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValue, "PrintJsonValue" }, // 1069049893
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintJsonValueArray, "PrintJsonValueArray" }, // 154113491
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringArrayEscaped, "PrintStringArrayEscaped" }, // 1198826835
		{ &Z_Construct_UFunction_ULowEntryJsonDebugLibrary_PrintStringEscaped, "PrintStringEscaped" }, // 3555389643
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Classes/LowEntryJsonDebugLibrary.h" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonDebugLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULowEntryJsonDebugLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics::ClassParams = {
		&ULowEntryJsonDebugLibrary::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULowEntryJsonDebugLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULowEntryJsonDebugLibrary, 3691153954);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULowEntryJsonDebugLibrary(Z_Construct_UClass_ULowEntryJsonDebugLibrary, &ULowEntryJsonDebugLibrary::StaticClass, TEXT("/Script/LowEntryJson"), TEXT("ULowEntryJsonDebugLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULowEntryJsonDebugLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
