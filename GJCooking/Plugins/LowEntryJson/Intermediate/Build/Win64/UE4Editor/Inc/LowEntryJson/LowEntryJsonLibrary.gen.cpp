// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LowEntryJson/Public/Classes/LowEntryJsonLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLowEntryJsonLibrary() {}
// Cross Module References
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonLibrary_NoRegister();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_LowEntryJson();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonArray_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonObject_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonValue_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get();
	LOWENTRYJSON_API UEnum* Z_Construct_UEnum_LowEntryJson_ELowEntryJsonType();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean();
	LOWENTRYJSON_API UEnum* Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue();
	LOWENTRYJSON_API UEnum* Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueFound();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString();
	LOWENTRYJSON_API UEnum* Z_Construct_UEnum_LowEntryJson_ELowEntryJsonParseResult();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonObjectIterator_NoRegister();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean();
	LOWENTRYJSON_API UEnum* Z_Construct_UEnum_LowEntryJson_ELowEntryJsonTypeFound();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString();
	LOWENTRYJSON_API UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile();
// End Cross Module References
	void ULowEntryJsonLibrary::StaticRegisterNativesULowEntryJsonLibrary()
	{
		UClass* Class = ULowEntryJsonLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Array_AddBoolean", &ULowEntryJsonLibrary::execArray_AddBoolean },
			{ "Array_AddByte", &ULowEntryJsonLibrary::execArray_AddByte },
			{ "Array_AddFloat", &ULowEntryJsonLibrary::execArray_AddFloat },
			{ "Array_AddInteger", &ULowEntryJsonLibrary::execArray_AddInteger },
			{ "Array_AddJsonArray", &ULowEntryJsonLibrary::execArray_AddJsonArray },
			{ "Array_AddJsonObject", &ULowEntryJsonLibrary::execArray_AddJsonObject },
			{ "Array_AddJsonValue", &ULowEntryJsonLibrary::execArray_AddJsonValue },
			{ "Array_AddNull", &ULowEntryJsonLibrary::execArray_AddNull },
			{ "Array_AddString", &ULowEntryJsonLibrary::execArray_AddString },
			{ "Array_Append", &ULowEntryJsonLibrary::execArray_Append },
			{ "Array_Clear", &ULowEntryJsonLibrary::execArray_Clear },
			{ "Array_Clone", &ULowEntryJsonLibrary::execArray_Clone },
			{ "Array_CreateFromBooleanArray", &ULowEntryJsonLibrary::execArray_CreateFromBooleanArray },
			{ "Array_CreateFromByteArray", &ULowEntryJsonLibrary::execArray_CreateFromByteArray },
			{ "Array_CreateFromFloatArray", &ULowEntryJsonLibrary::execArray_CreateFromFloatArray },
			{ "Array_CreateFromIntegerArray", &ULowEntryJsonLibrary::execArray_CreateFromIntegerArray },
			{ "Array_CreateFromJsonArrayArray", &ULowEntryJsonLibrary::execArray_CreateFromJsonArrayArray },
			{ "Array_CreateFromJsonObjectArray", &ULowEntryJsonLibrary::execArray_CreateFromJsonObjectArray },
			{ "Array_CreateFromJsonValueArray", &ULowEntryJsonLibrary::execArray_CreateFromJsonValueArray },
			{ "Array_CreateFromStringArray", &ULowEntryJsonLibrary::execArray_CreateFromStringArray },
			{ "Array_Expand", &ULowEntryJsonLibrary::execArray_Expand },
			{ "Array_Get", &ULowEntryJsonLibrary::execArray_Get },
			{ "Array_GetAsBooleanOrDefault", &ULowEntryJsonLibrary::execArray_GetAsBooleanOrDefault },
			{ "Array_GetAsByteOrDefault", &ULowEntryJsonLibrary::execArray_GetAsByteOrDefault },
			{ "Array_GetAsFloatOrDefault", &ULowEntryJsonLibrary::execArray_GetAsFloatOrDefault },
			{ "Array_GetAsIntegerOrDefault", &ULowEntryJsonLibrary::execArray_GetAsIntegerOrDefault },
			{ "Array_GetAsJsonArrayOrDefault", &ULowEntryJsonLibrary::execArray_GetAsJsonArrayOrDefault },
			{ "Array_GetAsJsonObjectOrDefault", &ULowEntryJsonLibrary::execArray_GetAsJsonObjectOrDefault },
			{ "Array_GetAsJsonValueOrDefault", &ULowEntryJsonLibrary::execArray_GetAsJsonValueOrDefault },
			{ "Array_GetAsStringOrDefault", &ULowEntryJsonLibrary::execArray_GetAsStringOrDefault },
			{ "Array_GetBoolean", &ULowEntryJsonLibrary::execArray_GetBoolean },
			{ "Array_GetByte", &ULowEntryJsonLibrary::execArray_GetByte },
			{ "Array_GetFloat", &ULowEntryJsonLibrary::execArray_GetFloat },
			{ "Array_GetInteger", &ULowEntryJsonLibrary::execArray_GetInteger },
			{ "Array_GetJsonArray", &ULowEntryJsonLibrary::execArray_GetJsonArray },
			{ "Array_GetJsonObject", &ULowEntryJsonLibrary::execArray_GetJsonObject },
			{ "Array_GetJsonValue", &ULowEntryJsonLibrary::execArray_GetJsonValue },
			{ "Array_GetNull", &ULowEntryJsonLibrary::execArray_GetNull },
			{ "Array_GetString", &ULowEntryJsonLibrary::execArray_GetString },
			{ "Array_InsertBoolean", &ULowEntryJsonLibrary::execArray_InsertBoolean },
			{ "Array_InsertByte", &ULowEntryJsonLibrary::execArray_InsertByte },
			{ "Array_InsertFloat", &ULowEntryJsonLibrary::execArray_InsertFloat },
			{ "Array_InsertInteger", &ULowEntryJsonLibrary::execArray_InsertInteger },
			{ "Array_InsertJsonArray", &ULowEntryJsonLibrary::execArray_InsertJsonArray },
			{ "Array_InsertJsonObject", &ULowEntryJsonLibrary::execArray_InsertJsonObject },
			{ "Array_InsertJsonValue", &ULowEntryJsonLibrary::execArray_InsertJsonValue },
			{ "Array_InsertNull", &ULowEntryJsonLibrary::execArray_InsertNull },
			{ "Array_InsertString", &ULowEntryJsonLibrary::execArray_InsertString },
			{ "Array_IsSet", &ULowEntryJsonLibrary::execArray_IsSet },
			{ "Array_Length", &ULowEntryJsonLibrary::execArray_Length },
			{ "Array_Merge", &ULowEntryJsonLibrary::execArray_Merge },
			{ "Array_Pure_Clone", &ULowEntryJsonLibrary::execArray_Pure_Clone },
			{ "Array_Pure_CreateFromBooleanArray", &ULowEntryJsonLibrary::execArray_Pure_CreateFromBooleanArray },
			{ "Array_Pure_CreateFromByteArray", &ULowEntryJsonLibrary::execArray_Pure_CreateFromByteArray },
			{ "Array_Pure_CreateFromFloatArray", &ULowEntryJsonLibrary::execArray_Pure_CreateFromFloatArray },
			{ "Array_Pure_CreateFromIntegerArray", &ULowEntryJsonLibrary::execArray_Pure_CreateFromIntegerArray },
			{ "Array_Pure_CreateFromJsonArrayArray", &ULowEntryJsonLibrary::execArray_Pure_CreateFromJsonArrayArray },
			{ "Array_Pure_CreateFromJsonObjectArray", &ULowEntryJsonLibrary::execArray_Pure_CreateFromJsonObjectArray },
			{ "Array_Pure_CreateFromJsonValueArray", &ULowEntryJsonLibrary::execArray_Pure_CreateFromJsonValueArray },
			{ "Array_Pure_CreateFromStringArray", &ULowEntryJsonLibrary::execArray_Pure_CreateFromStringArray },
			{ "Array_Pure_Merge", &ULowEntryJsonLibrary::execArray_Pure_Merge },
			{ "Array_Pure_ToJsonString", &ULowEntryJsonLibrary::execArray_Pure_ToJsonString },
			{ "Array_Resize", &ULowEntryJsonLibrary::execArray_Resize },
			{ "Array_SetBoolean", &ULowEntryJsonLibrary::execArray_SetBoolean },
			{ "Array_SetByte", &ULowEntryJsonLibrary::execArray_SetByte },
			{ "Array_SetFloat", &ULowEntryJsonLibrary::execArray_SetFloat },
			{ "Array_SetInteger", &ULowEntryJsonLibrary::execArray_SetInteger },
			{ "Array_SetJsonArray", &ULowEntryJsonLibrary::execArray_SetJsonArray },
			{ "Array_SetJsonObject", &ULowEntryJsonLibrary::execArray_SetJsonObject },
			{ "Array_SetJsonValue", &ULowEntryJsonLibrary::execArray_SetJsonValue },
			{ "Array_SetNull", &ULowEntryJsonLibrary::execArray_SetNull },
			{ "Array_SetString", &ULowEntryJsonLibrary::execArray_SetString },
			{ "Array_Shorten", &ULowEntryJsonLibrary::execArray_Shorten },
			{ "Array_ToJsonString", &ULowEntryJsonLibrary::execArray_ToJsonString },
			{ "Array_Unset", &ULowEntryJsonLibrary::execArray_Unset },
			{ "Json_ParseJsonString", &ULowEntryJsonLibrary::execJson_ParseJsonString },
			{ "Object_Append", &ULowEntryJsonLibrary::execObject_Append },
			{ "Object_Clear", &ULowEntryJsonLibrary::execObject_Clear },
			{ "Object_Clone", &ULowEntryJsonLibrary::execObject_Clone },
			{ "Object_CreateFromObjectEntryArray", &ULowEntryJsonLibrary::execObject_CreateFromObjectEntryArray },
			{ "Object_Get", &ULowEntryJsonLibrary::execObject_Get },
			{ "Object_GetAsBooleanOrDefault", &ULowEntryJsonLibrary::execObject_GetAsBooleanOrDefault },
			{ "Object_GetAsByteOrDefault", &ULowEntryJsonLibrary::execObject_GetAsByteOrDefault },
			{ "Object_GetAsFloatOrDefault", &ULowEntryJsonLibrary::execObject_GetAsFloatOrDefault },
			{ "Object_GetAsIntegerOrDefault", &ULowEntryJsonLibrary::execObject_GetAsIntegerOrDefault },
			{ "Object_GetAsJsonArrayOrDefault", &ULowEntryJsonLibrary::execObject_GetAsJsonArrayOrDefault },
			{ "Object_GetAsJsonObjectOrDefault", &ULowEntryJsonLibrary::execObject_GetAsJsonObjectOrDefault },
			{ "Object_GetAsJsonValueOrDefault", &ULowEntryJsonLibrary::execObject_GetAsJsonValueOrDefault },
			{ "Object_GetAsStringOrDefault", &ULowEntryJsonLibrary::execObject_GetAsStringOrDefault },
			{ "Object_GetBoolean", &ULowEntryJsonLibrary::execObject_GetBoolean },
			{ "Object_GetByte", &ULowEntryJsonLibrary::execObject_GetByte },
			{ "Object_GetFloat", &ULowEntryJsonLibrary::execObject_GetFloat },
			{ "Object_GetInteger", &ULowEntryJsonLibrary::execObject_GetInteger },
			{ "Object_GetJsonArray", &ULowEntryJsonLibrary::execObject_GetJsonArray },
			{ "Object_GetJsonObject", &ULowEntryJsonLibrary::execObject_GetJsonObject },
			{ "Object_GetJsonValue", &ULowEntryJsonLibrary::execObject_GetJsonValue },
			{ "Object_GetKeys", &ULowEntryJsonLibrary::execObject_GetKeys },
			{ "Object_GetNull", &ULowEntryJsonLibrary::execObject_GetNull },
			{ "Object_GetString", &ULowEntryJsonLibrary::execObject_GetString },
			{ "Object_GetValues", &ULowEntryJsonLibrary::execObject_GetValues },
			{ "Object_IsSet", &ULowEntryJsonLibrary::execObject_IsSet },
			{ "Object_Length", &ULowEntryJsonLibrary::execObject_Length },
			{ "Object_Merge", &ULowEntryJsonLibrary::execObject_Merge },
			{ "Object_Pure_Clone", &ULowEntryJsonLibrary::execObject_Pure_Clone },
			{ "Object_Pure_CreateFromObjectEntryArray", &ULowEntryJsonLibrary::execObject_Pure_CreateFromObjectEntryArray },
			{ "Object_Pure_Merge", &ULowEntryJsonLibrary::execObject_Pure_Merge },
			{ "Object_Pure_ToJsonString", &ULowEntryJsonLibrary::execObject_Pure_ToJsonString },
			{ "Object_SetBoolean", &ULowEntryJsonLibrary::execObject_SetBoolean },
			{ "Object_SetByte", &ULowEntryJsonLibrary::execObject_SetByte },
			{ "Object_SetFloat", &ULowEntryJsonLibrary::execObject_SetFloat },
			{ "Object_SetInteger", &ULowEntryJsonLibrary::execObject_SetInteger },
			{ "Object_SetJsonArray", &ULowEntryJsonLibrary::execObject_SetJsonArray },
			{ "Object_SetJsonObject", &ULowEntryJsonLibrary::execObject_SetJsonObject },
			{ "Object_SetJsonValue", &ULowEntryJsonLibrary::execObject_SetJsonValue },
			{ "Object_SetNull", &ULowEntryJsonLibrary::execObject_SetNull },
			{ "Object_SetString", &ULowEntryJsonLibrary::execObject_SetString },
			{ "Object_Sort", &ULowEntryJsonLibrary::execObject_Sort },
			{ "Object_ToJsonString", &ULowEntryJsonLibrary::execObject_ToJsonString },
			{ "Object_Unset", &ULowEntryJsonLibrary::execObject_Unset },
			{ "ObjectEntry_Pure_Create", &ULowEntryJsonLibrary::execObjectEntry_Pure_Create },
			{ "ObjectEntry_Pure_CreateFromBoolean", &ULowEntryJsonLibrary::execObjectEntry_Pure_CreateFromBoolean },
			{ "ObjectEntry_Pure_CreateFromByte", &ULowEntryJsonLibrary::execObjectEntry_Pure_CreateFromByte },
			{ "ObjectEntry_Pure_CreateFromFloat", &ULowEntryJsonLibrary::execObjectEntry_Pure_CreateFromFloat },
			{ "ObjectEntry_Pure_CreateFromInteger", &ULowEntryJsonLibrary::execObjectEntry_Pure_CreateFromInteger },
			{ "ObjectEntry_Pure_CreateFromJsonArray", &ULowEntryJsonLibrary::execObjectEntry_Pure_CreateFromJsonArray },
			{ "ObjectEntry_Pure_CreateFromJsonObject", &ULowEntryJsonLibrary::execObjectEntry_Pure_CreateFromJsonObject },
			{ "ObjectEntry_Pure_CreateFromNull", &ULowEntryJsonLibrary::execObjectEntry_Pure_CreateFromNull },
			{ "ObjectEntry_Pure_CreateFromString", &ULowEntryJsonLibrary::execObjectEntry_Pure_CreateFromString },
			{ "ObjectIterator_Clone", &ULowEntryJsonLibrary::execObjectIterator_Clone },
			{ "ObjectIterator_Create", &ULowEntryJsonLibrary::execObjectIterator_Create },
			{ "ObjectIterator_Get", &ULowEntryJsonLibrary::execObjectIterator_Get },
			{ "ObjectIterator_GetKeys", &ULowEntryJsonLibrary::execObjectIterator_GetKeys },
			{ "ObjectIterator_GetValues", &ULowEntryJsonLibrary::execObjectIterator_GetValues },
			{ "ObjectIterator_Length", &ULowEntryJsonLibrary::execObjectIterator_Length },
			{ "ReadEveryFiles", &ULowEntryJsonLibrary::execReadEveryFiles },
			{ "ReadFile2", &ULowEntryJsonLibrary::execReadFile2 },
			{ "Value_Clone", &ULowEntryJsonLibrary::execValue_Clone },
			{ "Value_Create", &ULowEntryJsonLibrary::execValue_Create },
			{ "Value_CreateFromBoolean", &ULowEntryJsonLibrary::execValue_CreateFromBoolean },
			{ "Value_CreateFromByte", &ULowEntryJsonLibrary::execValue_CreateFromByte },
			{ "Value_CreateFromFloat", &ULowEntryJsonLibrary::execValue_CreateFromFloat },
			{ "Value_CreateFromInteger", &ULowEntryJsonLibrary::execValue_CreateFromInteger },
			{ "Value_CreateFromJsonArray", &ULowEntryJsonLibrary::execValue_CreateFromJsonArray },
			{ "Value_CreateFromJsonObject", &ULowEntryJsonLibrary::execValue_CreateFromJsonObject },
			{ "Value_CreateFromNull", &ULowEntryJsonLibrary::execValue_CreateFromNull },
			{ "Value_CreateFromString", &ULowEntryJsonLibrary::execValue_CreateFromString },
			{ "Value_Get", &ULowEntryJsonLibrary::execValue_Get },
			{ "Value_GetAsBooleanOrDefault", &ULowEntryJsonLibrary::execValue_GetAsBooleanOrDefault },
			{ "Value_GetAsByteOrDefault", &ULowEntryJsonLibrary::execValue_GetAsByteOrDefault },
			{ "Value_GetAsFloatOrDefault", &ULowEntryJsonLibrary::execValue_GetAsFloatOrDefault },
			{ "Value_GetAsIntegerOrDefault", &ULowEntryJsonLibrary::execValue_GetAsIntegerOrDefault },
			{ "Value_GetAsJsonArrayOrDefault", &ULowEntryJsonLibrary::execValue_GetAsJsonArrayOrDefault },
			{ "Value_GetAsJsonObjectOrDefault", &ULowEntryJsonLibrary::execValue_GetAsJsonObjectOrDefault },
			{ "Value_GetAsStringOrDefault", &ULowEntryJsonLibrary::execValue_GetAsStringOrDefault },
			{ "Value_GetBoolean", &ULowEntryJsonLibrary::execValue_GetBoolean },
			{ "Value_GetByte", &ULowEntryJsonLibrary::execValue_GetByte },
			{ "Value_GetFloat", &ULowEntryJsonLibrary::execValue_GetFloat },
			{ "Value_GetInteger", &ULowEntryJsonLibrary::execValue_GetInteger },
			{ "Value_GetJsonArray", &ULowEntryJsonLibrary::execValue_GetJsonArray },
			{ "Value_GetJsonObject", &ULowEntryJsonLibrary::execValue_GetJsonObject },
			{ "Value_GetNull", &ULowEntryJsonLibrary::execValue_GetNull },
			{ "Value_GetString", &ULowEntryJsonLibrary::execValue_GetString },
			{ "Value_Pure_Clone", &ULowEntryJsonLibrary::execValue_Pure_Clone },
			{ "Value_Pure_Create", &ULowEntryJsonLibrary::execValue_Pure_Create },
			{ "Value_Pure_CreateFromBoolean", &ULowEntryJsonLibrary::execValue_Pure_CreateFromBoolean },
			{ "Value_Pure_CreateFromByte", &ULowEntryJsonLibrary::execValue_Pure_CreateFromByte },
			{ "Value_Pure_CreateFromFloat", &ULowEntryJsonLibrary::execValue_Pure_CreateFromFloat },
			{ "Value_Pure_CreateFromInteger", &ULowEntryJsonLibrary::execValue_Pure_CreateFromInteger },
			{ "Value_Pure_CreateFromJsonArray", &ULowEntryJsonLibrary::execValue_Pure_CreateFromJsonArray },
			{ "Value_Pure_CreateFromJsonObject", &ULowEntryJsonLibrary::execValue_Pure_CreateFromJsonObject },
			{ "Value_Pure_CreateFromNull", &ULowEntryJsonLibrary::execValue_Pure_CreateFromNull },
			{ "Value_Pure_CreateFromString", &ULowEntryJsonLibrary::execValue_Pure_CreateFromString },
			{ "Value_Set", &ULowEntryJsonLibrary::execValue_Set },
			{ "Value_SetBoolean", &ULowEntryJsonLibrary::execValue_SetBoolean },
			{ "Value_SetByte", &ULowEntryJsonLibrary::execValue_SetByte },
			{ "Value_SetFloat", &ULowEntryJsonLibrary::execValue_SetFloat },
			{ "Value_SetInteger", &ULowEntryJsonLibrary::execValue_SetInteger },
			{ "Value_SetJsonArray", &ULowEntryJsonLibrary::execValue_SetJsonArray },
			{ "Value_SetJsonObject", &ULowEntryJsonLibrary::execValue_SetJsonObject },
			{ "Value_SetNull", &ULowEntryJsonLibrary::execValue_SetNull },
			{ "Value_SetString", &ULowEntryJsonLibrary::execValue_SetString },
			{ "WrriteFile", &ULowEntryJsonLibrary::execWrriteFile },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventArray_AddBoolean_Parms
		{
			ULowEntryJsonArray* JsonArray;
			bool Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_AddBoolean_Parms*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_AddBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddBoolean_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Add Boolean" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds the given Boolean." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_AddBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_AddBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics
	{
		struct LowEntryJsonLibrary_eventArray_AddByte_Parms
		{
			ULowEntryJsonArray* JsonArray;
			uint8 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddByte_Parms, Value), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddByte_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Add Byte" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds the given Byte." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_AddByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_AddByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics
	{
		struct LowEntryJsonLibrary_eventArray_AddFloat_Parms
		{
			ULowEntryJsonArray* JsonArray;
			float Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddFloat_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddFloat_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Add Float" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds the given Float." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_AddFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_AddFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics
	{
		struct LowEntryJsonLibrary_eventArray_AddInteger_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddInteger_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddInteger_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Add Integer" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds the given Integer." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_AddInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_AddInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_AddJsonArray_Parms
		{
			ULowEntryJsonArray* JsonArray;
			ULowEntryJsonArray* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddJsonArray_Parms, Value), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddJsonArray_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Add Json Array" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds a copy of the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_AddJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_AddJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventArray_AddJsonObject_Parms
		{
			ULowEntryJsonArray* JsonArray;
			ULowEntryJsonObject* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddJsonObject_Parms, Value), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddJsonObject_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Add Json Object" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds a copy of the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_AddJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_AddJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics
	{
		struct LowEntryJsonLibrary_eventArray_AddJsonValue_Parms
		{
			ULowEntryJsonArray* JsonArray;
			ULowEntryJsonValue* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddJsonValue_Parms, Value), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddJsonValue_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Add Json Value" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds a copy of the value of the given Json Value." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_AddJsonValue", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_AddJsonValue_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics
	{
		struct LowEntryJsonLibrary_eventArray_AddNull_Parms
		{
			ULowEntryJsonArray* JsonArray;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddNull_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Add Null" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_AddNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_AddNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics
	{
		struct LowEntryJsonLibrary_eventArray_AddString_Parms
		{
			ULowEntryJsonArray* JsonArray;
			FString Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddString_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_AddString_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Add String" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds the given String." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_AddString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_AddString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Append_Parms
		{
			ULowEntryJsonArray* JsonArray;
			ULowEntryJsonArray* AppendJsonArray;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AppendJsonArray;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::NewProp_AppendJsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "AppendJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Append_Parms, AppendJsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Append_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::NewProp_AppendJsonArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Other" },
		{ "DisplayName", "Append" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds a copy of the entries of the given Json Array to this Json Array.\nThis is basically the same as Merge, except that this changes this Json Array instead of creating a new one, which causes this function to be more efficient and faster." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Append", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_Append_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Clear_Parms
		{
			ULowEntryJsonArray* JsonArray;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Clear_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Other" },
		{ "DisplayName", "Clear" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Removes all entries from this Json Array, making it an empty Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Clear", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_Clear_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Clone_Parms
		{
			ULowEntryJsonArray* JsonArray;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Clone_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Clone_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Clone" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with a copy of the data from the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Clone", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_Clone_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_CreateFromBooleanArray_Parms
		{
			TArray<bool> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromBooleanArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromBooleanArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Create Json Array (Boolean Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Booleans." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_CreateFromBooleanArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_CreateFromBooleanArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_CreateFromByteArray_Parms
		{
			TArray<uint8> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromByteArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromByteArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Create Json Array (Byte Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Bytes." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_CreateFromByteArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_CreateFromByteArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_CreateFromFloatArray_Parms
		{
			TArray<float> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromFloatArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromFloatArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Create Json Array (Float Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Floats." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_CreateFromFloatArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_CreateFromFloatArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_CreateFromIntegerArray_Parms
		{
			TArray<int32> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromIntegerArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromIntegerArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Create Json Array (Integer Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Integers." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_CreateFromIntegerArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_CreateFromIntegerArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_CreateFromJsonArrayArray_Parms
		{
			TArray<ULowEntryJsonArray*> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromJsonArrayArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromJsonArrayArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Create Json Array (Json Array Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with copies of the given Json Arrays." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_CreateFromJsonArrayArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_CreateFromJsonArrayArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_CreateFromJsonObjectArray_Parms
		{
			TArray<ULowEntryJsonObject*> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromJsonObjectArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromJsonObjectArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Create Json Array (Json Object Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with copies of the given Json Objects." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_CreateFromJsonObjectArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_CreateFromJsonObjectArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_CreateFromJsonValueArray_Parms
		{
			TArray<ULowEntryJsonValue*> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromJsonValueArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromJsonValueArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Create Json Array (Json Value Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with copies of the values of the given Json Values." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_CreateFromJsonValueArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_CreateFromJsonValueArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_CreateFromStringArray_Parms
		{
			TArray<FString> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromStringArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_CreateFromStringArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Create Json Array (String Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Strings." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_CreateFromStringArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_CreateFromStringArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Expand_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Size;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::NewProp_Size_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::NewProp_Size = { UE4CodeGen_Private::EPropertyClass::Int, "Size", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Expand_Parms, Size), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::NewProp_Size_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Expand_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::NewProp_Size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Other" },
		{ "DisplayName", "Expand" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Expands this Json Array to the given Size, new entries will be set to null.\nUnlike Resize, it will not shorten this Json Array if the current size is larger than the given size." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Expand", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_Expand_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Get_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonType Branch;
			FString ReturnStringValue;
			int32 ReturnIntegerValue;
			float ReturnFloatValue;
			uint8 ReturnByteValue;
			bool ReturnBooleanValue;
			ULowEntryJsonObject* ReturnJsonObjectValue;
			ULowEntryJsonArray* ReturnJsonArrayValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnJsonArrayValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnJsonObjectValue;
		static void NewProp_ReturnBooleanValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnBooleanValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnByteValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnFloatValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnIntegerValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnStringValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnJsonArrayValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnJsonArrayValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Get_Parms, ReturnJsonArrayValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnJsonObjectValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnJsonObjectValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Get_Parms, ReturnJsonObjectValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnBooleanValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_Get_Parms*)Obj)->ReturnBooleanValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnBooleanValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnBooleanValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_Get_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnBooleanValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnByteValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnByteValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Get_Parms, ReturnByteValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnFloatValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnFloatValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Get_Parms, ReturnFloatValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnIntegerValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnIntegerValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Get_Parms, ReturnIntegerValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnStringValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnStringValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Get_Parms, ReturnStringValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Get_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Get_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Get_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnJsonArrayValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnJsonObjectValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnBooleanValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnByteValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnFloatValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnIntegerValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_ReturnStringValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value and the type of the value, returns as a None value if no entry with the given key exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Get", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_Get_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetAsBooleanOrDefault_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			bool Default;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static void NewProp_Default_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_GetAsBooleanOrDefault_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_GetAsBooleanOrDefault_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Default_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_GetAsBooleanOrDefault_Parms*)Obj)->Default = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Bool, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_GetAsBooleanOrDefault_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Default_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsBooleanOrDefault_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsBooleanOrDefault_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get As Boolean" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Boolean, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetAsBooleanOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_GetAsBooleanOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetAsByteOrDefault_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			uint8 Default;
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsByteOrDefault_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Byte, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsByteOrDefault_Parms, Default), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsByteOrDefault_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsByteOrDefault_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get As Byte" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Byte, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetAsByteOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_GetAsByteOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetAsFloatOrDefault_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			float Default;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsFloatOrDefault_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Float, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsFloatOrDefault_Parms, Default), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsFloatOrDefault_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsFloatOrDefault_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get As Float" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Float, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetAsFloatOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_GetAsFloatOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetAsIntegerOrDefault_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			int32 Default;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsIntegerOrDefault_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Int, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsIntegerOrDefault_Parms, Default), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsIntegerOrDefault_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsIntegerOrDefault_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get As Integer" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as an Integer, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetAsIntegerOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_GetAsIntegerOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetAsJsonArrayOrDefault_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ULowEntryJsonArray* Default;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonArrayOrDefault_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Object, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonArrayOrDefault_Parms, Default), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonArrayOrDefault_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonArrayOrDefault_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get As Json Array" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Array, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetAsJsonArrayOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_GetAsJsonArrayOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetAsJsonObjectOrDefault_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ULowEntryJsonObject* Default;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonObjectOrDefault_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Object, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonObjectOrDefault_Parms, Default), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonObjectOrDefault_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonObjectOrDefault_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get As Json Object" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Object, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetAsJsonObjectOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_GetAsJsonObjectOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetAsJsonValueOrDefault_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ULowEntryJsonValue* Default;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonValueOrDefault_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Object, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonValueOrDefault_Parms, Default), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonValueOrDefault_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsJsonValueOrDefault_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get As Json Value" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Value, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetAsJsonValueOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_GetAsJsonValueOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetAsStringOrDefault_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			FString Default;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsStringOrDefault_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Str, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsStringOrDefault_Parms, Default), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsStringOrDefault_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetAsStringOrDefault_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get As String" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a String, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetAsStringOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_GetAsStringOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetBoolean_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonValueAndTypeFound Branch;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_GetBoolean_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_GetBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetBoolean_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetBoolean_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetBoolean_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get Boolean" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Boolean if an entry with the given key exists and if the value is a Boolean." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_GetBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetByte_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonValueAndTypeFound Branch;
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetByte_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetByte_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetByte_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetByte_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get Byte" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Byte if an entry with the given key exists and if the value is a possible Byte (Integer between 0 and 255)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_GetByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetFloat_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonValueAndTypeFound Branch;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetFloat_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetFloat_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetFloat_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetFloat_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get Float" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Float if an entry with the given key exists and if the value is a Float." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_GetFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetInteger_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonValueAndTypeFound Branch;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetInteger_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetInteger_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetInteger_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetInteger_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get Integer" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as an Integer if an entry with the given key exists and if the value is a possible Integer (Float with less than 0.001 away from a whole number)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_GetInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetJsonArray_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonValueAndTypeFound Branch;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonArray_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonArray_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonArray_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get Json Array" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Array if an entry with the given key exists and if the value is a Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_GetJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetJsonObject_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonValueAndTypeFound Branch;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonObject_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonObject_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonObject_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonObject_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get Json Object" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Object if an entry with the given key exists and if the value is a Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_GetJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetJsonValue_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonValueFound Branch;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonValue_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonValue_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonValue_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetJsonValue_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get Json Value" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Value if an entry with the given index exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetJsonValue", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_GetJsonValue_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetNull_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonValueAndTypeFound Branch;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetNull_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetNull_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetNull_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get Null" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Shows you if an entry with the given key exists and if the value is null or not." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_GetNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics
	{
		struct LowEntryJsonLibrary_eventArray_GetString_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ELowEntryJsonValueAndTypeFound Branch;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetString_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetString_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_GetString_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Get String" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a String if an entry with the given key exists and if the value is a String." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_GetString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventArray_GetString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventArray_InsertBoolean_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			bool Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_InsertBoolean_Parms*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_InsertBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertBoolean_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertBoolean_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Insert Boolean" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Inserts the given Boolean before the given index, making this the new value at the given index, and increasing all indexes after this by one.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_InsertBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_InsertBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics
	{
		struct LowEntryJsonLibrary_eventArray_InsertByte_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			uint8 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertByte_Parms, Value), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertByte_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertByte_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Insert Byte" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Inserts the given Byte before the given index, making this the new value at the given index, and increasing all indexes after this by one.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_InsertByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_InsertByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics
	{
		struct LowEntryJsonLibrary_eventArray_InsertFloat_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			float Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertFloat_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertFloat_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertFloat_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Insert Float" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Inserts the given Float before the given index, making this the new value at the given index, and increasing all indexes after this by one.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_InsertFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_InsertFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics
	{
		struct LowEntryJsonLibrary_eventArray_InsertInteger_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			int32 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertInteger_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertInteger_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertInteger_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Insert Integer" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Inserts the given Integer before the given index, making this the new value at the given index, and increasing all indexes after this by one.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_InsertInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_InsertInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_InsertJsonArray_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ULowEntryJsonArray* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertJsonArray_Parms, Value), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertJsonArray_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertJsonArray_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Insert Json Array" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Inserts a copy of the given Json Array before the given index, making this the new value at the given index, and increasing all indexes after this by one.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_InsertJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_InsertJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventArray_InsertJsonObject_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ULowEntryJsonObject* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertJsonObject_Parms, Value), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertJsonObject_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertJsonObject_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Insert Json Object" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Inserts a copy of the given Json Object before the given index, making this the new value at the given index, and increasing all indexes after this by one.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_InsertJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_InsertJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics
	{
		struct LowEntryJsonLibrary_eventArray_InsertJsonValue_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ULowEntryJsonValue* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertJsonValue_Parms, Value), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertJsonValue_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertJsonValue_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Insert Json Value" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Inserts a copy of the value of the given Json Value before the given index, making this the new value at the given index, and increasing all indexes after this by one.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_InsertJsonValue", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_InsertJsonValue_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics
	{
		struct LowEntryJsonLibrary_eventArray_InsertNull_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertNull_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertNull_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Insert Null" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Inserts null before the given index, making this the new value at the given index, and increasing all indexes after this by one.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_InsertNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_InsertNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics
	{
		struct LowEntryJsonLibrary_eventArray_InsertString_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			FString Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertString_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertString_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_InsertString_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Insert String" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Inserts the given String before the given index, making this the new value at the given index, and increasing all indexes after this by one.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_InsertString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_InsertString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics
	{
		struct LowEntryJsonLibrary_eventArray_IsSet_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_IsSet_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_IsSet_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_IsSet_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_IsSet_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Is Set" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns true if this Json Array has a value set on the given index." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_IsSet", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_IsSet_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Length_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Length_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Length_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Other" },
		{ "DisplayName", "Length" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the number of entries in the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Length", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_Length_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Merge_Parms
		{
			ULowEntryJsonArray* JsonArray1;
			ULowEntryJsonArray* JsonArray2;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray2;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray1;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Merge_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::NewProp_JsonArray2 = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray2", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Merge_Parms, JsonArray2), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::NewProp_JsonArray1 = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray1", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Merge_Parms, JsonArray1), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::NewProp_JsonArray2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::NewProp_JsonArray1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Callable)" },
		{ "DisplayName", "Merge" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with a copy of the data from the two given Json Arrays." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Merge", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_Merge_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_Clone_Parms
		{
			ULowEntryJsonArray* JsonArray;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_Clone_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_Clone_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Clone" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with a copy of the data from the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_Clone", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_Pure_Clone_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_CreateFromBooleanArray_Parms
		{
			TArray<bool> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromBooleanArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromBooleanArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Create Json Array (Boolean Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Booleans." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_CreateFromBooleanArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(LowEntryJsonLibrary_eventArray_Pure_CreateFromBooleanArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_CreateFromByteArray_Parms
		{
			TArray<uint8> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromByteArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromByteArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Create Json Array (Byte Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Bytes." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_CreateFromByteArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(LowEntryJsonLibrary_eventArray_Pure_CreateFromByteArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_CreateFromFloatArray_Parms
		{
			TArray<float> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromFloatArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromFloatArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Create Json Array (Float Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Floats." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_CreateFromFloatArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(LowEntryJsonLibrary_eventArray_Pure_CreateFromFloatArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_CreateFromIntegerArray_Parms
		{
			TArray<int32> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromIntegerArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromIntegerArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Create Json Array (Integer Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Integers." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_CreateFromIntegerArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(LowEntryJsonLibrary_eventArray_Pure_CreateFromIntegerArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonArrayArray_Parms
		{
			TArray<ULowEntryJsonArray*> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonArrayArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonArrayArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Create Json Array (Json Array Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with copies of the given Json Arrays." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_CreateFromJsonArrayArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonArrayArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonObjectArray_Parms
		{
			TArray<ULowEntryJsonObject*> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonObjectArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonObjectArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Create Json Array (Json Object Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with copies of the given Json Objects." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_CreateFromJsonObjectArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonObjectArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonValueArray_Parms
		{
			TArray<ULowEntryJsonValue*> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonValueArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonValueArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Create Json Array (Json Value Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with copies of the values of the given Json Values." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_CreateFromJsonValueArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(LowEntryJsonLibrary_eventArray_Pure_CreateFromJsonValueArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_CreateFromStringArray_Parms
		{
			TArray<FString> Value;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromStringArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Array, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_CreateFromStringArray_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::NewProp_Value_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::NewProp_Value_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Create Json Array (String Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with the given Strings." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_CreateFromStringArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(LowEntryJsonLibrary_eventArray_Pure_CreateFromStringArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_Merge_Parms
		{
			ULowEntryJsonArray* JsonArray1;
			ULowEntryJsonArray* JsonArray2;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray2;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray1;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_Merge_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::NewProp_JsonArray2 = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray2", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_Merge_Parms, JsonArray2), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::NewProp_JsonArray1 = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray1", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_Merge_Parms, JsonArray1), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::NewProp_JsonArray2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::NewProp_JsonArray1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Create (Pure)" },
		{ "DisplayName", "Merge" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Array and fills it with a copy of the data from the two given Json Arrays." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_Merge", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_Pure_Merge_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Pure_ToJsonString_Parms
		{
			ULowEntryJsonArray* JsonArray;
			bool PrettyPrint;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrettyPrint_MetaData[];
#endif
		static void NewProp_PrettyPrint_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_PrettyPrint;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_ToJsonString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_PrettyPrint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_PrettyPrint_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_Pure_ToJsonString_Parms*)Obj)->PrettyPrint = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_PrettyPrint = { UE4CodeGen_Private::EPropertyClass::Bool, "PrettyPrint", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_Pure_ToJsonString_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_PrettyPrint_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_PrettyPrint_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_PrettyPrint_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Pure_ToJsonString_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_PrettyPrint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Json String|Stringify" },
		{ "CPP_Default_PrettyPrint", "true" },
		{ "DisplayName", "To Json String (Json Array) (Pure)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a Json String from the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Pure_ToJsonString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventArray_Pure_ToJsonString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Resize_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Size;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::NewProp_Size_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::NewProp_Size = { UE4CodeGen_Private::EPropertyClass::Int, "Size", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Resize_Parms, Size), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::NewProp_Size_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Resize_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::NewProp_Size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Other" },
		{ "DisplayName", "Resize" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Expands or shortens this Json Array to the given Size, new entries will be set to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Resize", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_Resize_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventArray_SetBoolean_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			bool Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_SetBoolean_Parms*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_SetBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetBoolean_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetBoolean_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Set Boolean" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the given index to the given Boolean.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_SetBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_SetBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics
	{
		struct LowEntryJsonLibrary_eventArray_SetByte_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			uint8 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetByte_Parms, Value), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetByte_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetByte_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Set Byte" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the given index to the given Byte.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_SetByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_SetByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics
	{
		struct LowEntryJsonLibrary_eventArray_SetFloat_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			float Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetFloat_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetFloat_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetFloat_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Set Float" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the given index to the given Float.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_SetFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_SetFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics
	{
		struct LowEntryJsonLibrary_eventArray_SetInteger_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			int32 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetInteger_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetInteger_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetInteger_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Set Integer" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the given index to the given Integer.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_SetInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_SetInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventArray_SetJsonArray_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ULowEntryJsonArray* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetJsonArray_Parms, Value), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetJsonArray_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetJsonArray_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Set Json Array" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the given index to a copy of the given Json Array.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_SetJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_SetJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventArray_SetJsonObject_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ULowEntryJsonObject* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetJsonObject_Parms, Value), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetJsonObject_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetJsonObject_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Set Json Object" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the given index to a copy of the given Json Object.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_SetJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_SetJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics
	{
		struct LowEntryJsonLibrary_eventArray_SetJsonValue_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			ULowEntryJsonValue* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetJsonValue_Parms, Value), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetJsonValue_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetJsonValue_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Set Json Value" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the given index to a copy of the value of the given Json Value.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_SetJsonValue", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_SetJsonValue_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics
	{
		struct LowEntryJsonLibrary_eventArray_SetNull_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetNull_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetNull_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Set Null" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the given index to null.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null.\n\nNote: This doesn't remove an entry because null is a valid value in Json, to remove an entry from a Json Array, use Unset." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_SetNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_SetNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics
	{
		struct LowEntryJsonLibrary_eventArray_SetString_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			FString Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetString_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetString_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_SetString_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "DisplayName", "Set String" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the given index to the given String.\nIf the given index is outside of this Json Array, it will automatically expand this Json Array and set the values of all new entries to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_SetString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_SetString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Shorten_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Size;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::NewProp_Size_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::NewProp_Size = { UE4CodeGen_Private::EPropertyClass::Int, "Size", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Shorten_Parms, Size), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::NewProp_Size_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Shorten_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::NewProp_Size,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Other" },
		{ "DisplayName", "Shorten" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Shortens this Json Array to the given Size.\nUnlike Resize, it will not expand this Json Array if the current size is smaller than the given size." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Shorten", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_Shorten_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics
	{
		struct LowEntryJsonLibrary_eventArray_ToJsonString_Parms
		{
			ULowEntryJsonArray* JsonArray;
			bool PrettyPrint;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrettyPrint_MetaData[];
#endif
		static void NewProp_PrettyPrint_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_PrettyPrint;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_ToJsonString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_PrettyPrint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_PrettyPrint_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventArray_ToJsonString_Parms*)Obj)->PrettyPrint = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_PrettyPrint = { UE4CodeGen_Private::EPropertyClass::Bool, "PrettyPrint", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventArray_ToJsonString_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_PrettyPrint_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_PrettyPrint_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_PrettyPrint_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_ToJsonString_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_PrettyPrint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Json String|Stringify" },
		{ "CPP_Default_PrettyPrint", "true" },
		{ "DisplayName", "To Json String (Json Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a Json String from the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_ToJsonString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_ToJsonString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics
	{
		struct LowEntryJsonLibrary_eventArray_Unset_Parms
		{
			ULowEntryJsonArray* JsonArray;
			int32 Index;
			int32 Count;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Count_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Count = { UE4CodeGen_Private::EPropertyClass::Int, "Count", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Unset_Parms, Count), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Count_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Unset_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_JsonArray = { UE4CodeGen_Private::EPropertyClass::Object, "JsonArray", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventArray_Unset_Parms, JsonArray), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::NewProp_JsonArray,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Array|Field" },
		{ "CPP_Default_Count", "1" },
		{ "DisplayName", "Unset" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "If the given index(es) exists, it or they will be removed from this Json Array, higher indexes will be reduced by the amount of entries deleted." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Array_Unset", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventArray_Unset_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics
	{
		struct LowEntryJsonLibrary_eventJson_ParseJsonString_Parms
		{
			FString JsonString;
			ULowEntryJsonObject* ReturnJsonObjectValue;
			ULowEntryJsonArray* ReturnJsonArrayValue;
			ELowEntryJsonParseResult Branch;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnJsonArrayValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnJsonObjectValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JsonString_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_JsonString;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventJson_ParseJsonString_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonParseResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_ReturnJsonArrayValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnJsonArrayValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventJson_ParseJsonString_Parms, ReturnJsonArrayValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_ReturnJsonObjectValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnJsonObjectValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventJson_ParseJsonString_Parms, ReturnJsonObjectValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_JsonString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_JsonString = { UE4CodeGen_Private::EPropertyClass::Str, "JsonString", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventJson_ParseJsonString_Parms, JsonString), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_JsonString_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_JsonString_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_ReturnJsonArrayValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_ReturnJsonObjectValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::NewProp_JsonString,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Json String|Parse" },
		{ "DisplayName", "Parse Json String" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Parses a Json String into a Json Object or a Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Json_ParseJsonString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventJson_ParseJsonString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Append_Parms
		{
			ULowEntryJsonObject* JsonObject;
			ULowEntryJsonObject* AppendJsonObject;
			bool OverrideDuplicates;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideDuplicates_MetaData[];
#endif
		static void NewProp_OverrideDuplicates_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_OverrideDuplicates;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AppendJsonObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_OverrideDuplicates_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_OverrideDuplicates_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_Append_Parms*)Obj)->OverrideDuplicates = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_OverrideDuplicates = { UE4CodeGen_Private::EPropertyClass::Bool, "OverrideDuplicates", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_Append_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_OverrideDuplicates_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_OverrideDuplicates_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_OverrideDuplicates_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_AppendJsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "AppendJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Append_Parms, AppendJsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Append_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_OverrideDuplicates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_AppendJsonObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Other" },
		{ "CPP_Default_OverrideDuplicates", "true" },
		{ "DisplayName", "Append" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Adds a copy of the entries of the given Json Object to this Json Object.\nThis is basically the same as Merge, except that this changes this Json Object instead of creating a new one, which causes this function to be more efficient and faster.\nWhen the appended Json Object contains a key that this Json Object already contains, the key in this Json Object will be set to the value that has been set in the given Json Object if OverrideDuplicates is true." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Append", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_Append_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Clear_Parms
		{
			ULowEntryJsonObject* JsonObject;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Clear_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Other" },
		{ "DisplayName", "Clear" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Removes all entries from this Json Object, making it an empty Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Clear", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_Clear_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Clone_Parms
		{
			ULowEntryJsonObject* JsonObject;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Clone_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Clone_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create (Callable)" },
		{ "DisplayName", "Clone" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object and fills it with a copy of the data from the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Clone", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_Clone_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics
	{
		struct LowEntryJsonLibrary_eventObject_CreateFromObjectEntryArray_Parms
		{
			TArray<ULowEntryJsonObjectEntry*> Array;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Array_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Array;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Array_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_CreateFromObjectEntryArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::NewProp_Array_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::NewProp_Array = { UE4CodeGen_Private::EPropertyClass::Array, "Array", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_CreateFromObjectEntryArray_Parms, Array), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::NewProp_Array_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::NewProp_Array_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::NewProp_Array_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "Array", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::NewProp_Array,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::NewProp_Array_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create (Callable)" },
		{ "DisplayName", "Create Json Object (Json Object Entry Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_CreateFromObjectEntryArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_CreateFromObjectEntryArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Get_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonType Branch;
			FString ReturnStringValue;
			int32 ReturnIntegerValue;
			float ReturnFloatValue;
			uint8 ReturnByteValue;
			bool ReturnBooleanValue;
			ULowEntryJsonObject* ReturnJsonObjectValue;
			ULowEntryJsonArray* ReturnJsonArrayValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnJsonArrayValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnJsonObjectValue;
		static void NewProp_ReturnBooleanValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnBooleanValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnByteValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnFloatValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnIntegerValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnStringValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnJsonArrayValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnJsonArrayValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Get_Parms, ReturnJsonArrayValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnJsonObjectValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnJsonObjectValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Get_Parms, ReturnJsonObjectValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnBooleanValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_Get_Parms*)Obj)->ReturnBooleanValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnBooleanValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnBooleanValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_Get_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnBooleanValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnByteValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnByteValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Get_Parms, ReturnByteValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnFloatValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnFloatValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Get_Parms, ReturnFloatValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnIntegerValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnIntegerValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Get_Parms, ReturnIntegerValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnStringValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnStringValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Get_Parms, ReturnStringValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Get_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Get_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Get_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnJsonArrayValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnJsonObjectValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnBooleanValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnByteValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnFloatValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnIntegerValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_ReturnStringValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value and the type of the value, returns as a None value if no entry with the given key exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Get", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_Get_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetAsBooleanOrDefault_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			bool Default;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static void NewProp_Default_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_GetAsBooleanOrDefault_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_GetAsBooleanOrDefault_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Default_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_GetAsBooleanOrDefault_Parms*)Obj)->Default = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Bool, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_GetAsBooleanOrDefault_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Default_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsBooleanOrDefault_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsBooleanOrDefault_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get As Boolean" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Boolean, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetAsBooleanOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_GetAsBooleanOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetAsByteOrDefault_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			uint8 Default;
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsByteOrDefault_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Byte, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsByteOrDefault_Parms, Default), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsByteOrDefault_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsByteOrDefault_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get As Byte" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Byte, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetAsByteOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_GetAsByteOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetAsFloatOrDefault_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			float Default;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsFloatOrDefault_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Float, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsFloatOrDefault_Parms, Default), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsFloatOrDefault_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsFloatOrDefault_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get As Float" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Float, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetAsFloatOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_GetAsFloatOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetAsIntegerOrDefault_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			int32 Default;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsIntegerOrDefault_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Int, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsIntegerOrDefault_Parms, Default), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsIntegerOrDefault_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsIntegerOrDefault_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get As Integer" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as an Integer, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetAsIntegerOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_GetAsIntegerOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetAsJsonArrayOrDefault_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ULowEntryJsonArray* Default;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonArrayOrDefault_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Object, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonArrayOrDefault_Parms, Default), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonArrayOrDefault_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonArrayOrDefault_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get As Json Array" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Array, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetAsJsonArrayOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_GetAsJsonArrayOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetAsJsonObjectOrDefault_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ULowEntryJsonObject* Default;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonObjectOrDefault_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Object, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonObjectOrDefault_Parms, Default), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonObjectOrDefault_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonObjectOrDefault_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get As Json Object" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Object, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetAsJsonObjectOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_GetAsJsonObjectOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetAsJsonValueOrDefault_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ULowEntryJsonValue* Default;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonValueOrDefault_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Object, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonValueOrDefault_Parms, Default), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonValueOrDefault_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsJsonValueOrDefault_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get As Json Value" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Value, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetAsJsonValueOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_GetAsJsonValueOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetAsStringOrDefault_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			FString Default;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Default;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsStringOrDefault_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Str, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsStringOrDefault_Parms, Default), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Default_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsStringOrDefault_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetAsStringOrDefault_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get As String" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a String, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetAsStringOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_GetAsStringOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetBoolean_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonValueAndTypeFound Branch;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_GetBoolean_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_GetBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetBoolean_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetBoolean_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetBoolean_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get Boolean" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Boolean if an entry with the given key exists and if the value is a Boolean." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_GetBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetByte_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonValueAndTypeFound Branch;
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetByte_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetByte_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetByte_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetByte_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get Byte" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Byte if an entry with the given key exists and if the value is a possible Byte (Integer between 0 and 255)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_GetByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetFloat_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonValueAndTypeFound Branch;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetFloat_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetFloat_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetFloat_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetFloat_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get Float" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Float if an entry with the given key exists and if the value is a Float." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_GetFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetInteger_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonValueAndTypeFound Branch;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetInteger_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetInteger_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetInteger_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetInteger_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get Integer" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as an Integer if an entry with the given key exists and if the value is a possible Integer (Float with less than 0.001 away from a whole number)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_GetInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetJsonArray_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonValueAndTypeFound Branch;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonArray_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonArray_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonArray_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get Json Array" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Array if an entry with the given key exists and if the value is a Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_GetJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetJsonObject_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonValueAndTypeFound Branch;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonObject_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonObject_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonObject_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonObject_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get Json Object" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Object if an entry with the given key exists and if the value is a Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_GetJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetJsonValue_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonValueFound Branch;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonValue_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonValue_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonValue_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetJsonValue_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get Json Value" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Value if an entry with the given key exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetJsonValue", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_GetJsonValue_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetKeys_Parms
		{
			ULowEntryJsonObject* JsonObject;
			TArray<FString> ReturnValue;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Array, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetKeys_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::NewProp_ReturnValue_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetKeys_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Other" },
		{ "DisplayName", "Get Keys" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a String Array and fills it with the keys present in the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetKeys", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_GetKeys_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetNull_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonValueAndTypeFound Branch;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetNull_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetNull_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetNull_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get Null" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Shows you if an entry with the given key exists and if the value is null or not." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_GetNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetString_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ELowEntryJsonValueAndTypeFound Branch;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetString_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueAndTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetString_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetString_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Get String" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a String if an entry with the given key exists and if the value is a String." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObject_GetString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics
	{
		struct LowEntryJsonLibrary_eventObject_GetValues_Parms
		{
			ULowEntryJsonObject* JsonObject;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetValues_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_GetValues_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Other" },
		{ "DisplayName", "Get Values" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a Json Array and fills it with the values present in the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_GetValues", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_GetValues_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics
	{
		struct LowEntryJsonLibrary_eventObject_IsSet_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_IsSet_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_IsSet_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_IsSet_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_IsSet_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Is Set" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns true if this Json Object has an entry with the given key." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_IsSet", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_IsSet_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Length_Parms
		{
			ULowEntryJsonObject* JsonObject;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Length_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Length_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Other" },
		{ "DisplayName", "Length" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the number of entries in the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Length", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_Length_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Merge_Parms
		{
			ULowEntryJsonObject* JsonObject1;
			ULowEntryJsonObject* JsonObject2;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject2;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject1;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Merge_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::NewProp_JsonObject2 = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject2", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Merge_Parms, JsonObject2), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::NewProp_JsonObject1 = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject1", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Merge_Parms, JsonObject1), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::NewProp_JsonObject2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::NewProp_JsonObject1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create (Callable)" },
		{ "DisplayName", "Merge" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object and fills it with a copy of the data from the two given Json Objects.\nWhen both of the two given Json Objects have a value set on same key, the key in the new Json Object will be set to the value that has been set in Json Object 2." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Merge", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_Merge_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Pure_Clone_Parms
		{
			ULowEntryJsonObject* JsonObject;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Pure_Clone_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Pure_Clone_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create (Pure)" },
		{ "DisplayName", "Clone" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object and fills it with a copy of the data from the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Pure_Clone", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_Pure_Clone_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Pure_CreateFromObjectEntryArray_Parms
		{
			TArray<ULowEntryJsonObjectEntry*> Array;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Array_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Array;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Array_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Pure_CreateFromObjectEntryArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::NewProp_Array_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::NewProp_Array = { UE4CodeGen_Private::EPropertyClass::Array, "Array", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000008000182, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Pure_CreateFromObjectEntryArray_Parms, Array), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::NewProp_Array_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::NewProp_Array_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::NewProp_Array_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "Array", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::NewProp_Array,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::NewProp_Array_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create (Pure)" },
		{ "DisplayName", "Create Json Object (Json Object Entry Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Pure_CreateFromObjectEntryArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14422401, sizeof(LowEntryJsonLibrary_eventObject_Pure_CreateFromObjectEntryArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Pure_Merge_Parms
		{
			ULowEntryJsonObject* JsonObject1;
			ULowEntryJsonObject* JsonObject2;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject2;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject1;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Pure_Merge_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::NewProp_JsonObject2 = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject2", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Pure_Merge_Parms, JsonObject2), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::NewProp_JsonObject1 = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject1", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Pure_Merge_Parms, JsonObject1), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::NewProp_JsonObject2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::NewProp_JsonObject1,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create (Pure)" },
		{ "DisplayName", "Merge" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object and fills it with a copy of the data from the two given Json Objects.\nWhen both of the two given Json Objects have a value set on same key, the key in the new Json Object will be set to the value that has been set in Json Object 2." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Pure_Merge", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_Pure_Merge_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Pure_ToJsonString_Parms
		{
			ULowEntryJsonObject* JsonObject;
			bool PrettyPrint;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrettyPrint_MetaData[];
#endif
		static void NewProp_PrettyPrint_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_PrettyPrint;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Pure_ToJsonString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_PrettyPrint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_PrettyPrint_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_Pure_ToJsonString_Parms*)Obj)->PrettyPrint = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_PrettyPrint = { UE4CodeGen_Private::EPropertyClass::Bool, "PrettyPrint", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_Pure_ToJsonString_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_PrettyPrint_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_PrettyPrint_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_PrettyPrint_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Pure_ToJsonString_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_PrettyPrint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Json String|Stringify" },
		{ "CPP_Default_PrettyPrint", "true" },
		{ "DisplayName", "To Json String (Json Object) (Pure)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a Json String from the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Pure_ToJsonString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObject_Pure_ToJsonString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventObject_SetBoolean_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			bool Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_SetBoolean_Parms*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_SetBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetBoolean_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetBoolean_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Set Boolean" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the entry of the given key to the given Boolean.\nA new entry will be created if no entry with the given key yet exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_SetBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_SetBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics
	{
		struct LowEntryJsonLibrary_eventObject_SetByte_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			uint8 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetByte_Parms, Value), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetByte_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetByte_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Set Byte" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the entry of the given key to the given Byte.\nA new entry will be created if no entry with the given key yet exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_SetByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_SetByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics
	{
		struct LowEntryJsonLibrary_eventObject_SetFloat_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			float Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetFloat_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetFloat_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetFloat_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Set Float" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the entry of the given key to the given Float.\nA new entry will be created if no entry with the given key yet exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_SetFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_SetFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics
	{
		struct LowEntryJsonLibrary_eventObject_SetInteger_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			int32 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetInteger_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetInteger_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetInteger_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Set Integer" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the entry of the given key to the given Integer.\nA new entry will be created if no entry with the given key yet exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_SetInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_SetInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventObject_SetJsonArray_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ULowEntryJsonArray* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetJsonArray_Parms, Value), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetJsonArray_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetJsonArray_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Set Json Array" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the entry of the given key to a copy of the given Json Array.\nA new entry will be created if no entry with the given key yet exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_SetJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_SetJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventObject_SetJsonObject_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ULowEntryJsonObject* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetJsonObject_Parms, Value), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetJsonObject_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetJsonObject_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Set Json Object" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the entry of the given key to a copy of the given Json Object.\nA new entry will be created if no entry with the given key yet exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_SetJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_SetJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics
	{
		struct LowEntryJsonLibrary_eventObject_SetJsonValue_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			ULowEntryJsonValue* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetJsonValue_Parms, Value), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetJsonValue_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetJsonValue_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Set Json Value" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the entry of the given key to a copy of the value of the given Json Value.\nA new entry will be created if no entry with the given key yet exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_SetJsonValue", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_SetJsonValue_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics
	{
		struct LowEntryJsonLibrary_eventObject_SetNull_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetNull_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetNull_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Set Null" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the entry of the given key to null.\nA new entry will be created if no entry with the given key yet exists.\n\nNote: This doesn't remove an entry because null is a valid value in Json, to remove an entry from a Json Object, use Unset." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_SetNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_SetNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics
	{
		struct LowEntryJsonLibrary_eventObject_SetString_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
			FString Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetString_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetString_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_SetString_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Set String" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of the entry of the given key to the given String.\nA new entry will be created if no entry with the given key yet exists." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_SetString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_SetString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Sort_Parms
		{
			ULowEntryJsonObject* JsonObject;
			bool Reversed;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Reversed_MetaData[];
#endif
		static void NewProp_Reversed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Reversed;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::NewProp_Reversed_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::NewProp_Reversed_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_Sort_Parms*)Obj)->Reversed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::NewProp_Reversed = { UE4CodeGen_Private::EPropertyClass::Bool, "Reversed", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_Sort_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::NewProp_Reversed_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::NewProp_Reversed_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::NewProp_Reversed_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Sort_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::NewProp_Reversed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Other" },
		{ "CPP_Default_Reversed", "false" },
		{ "DisplayName", "Sort" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sorts the entries on the keys, A to Z if Reversed is false, Z to A if Reversed is true." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Sort", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_Sort_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics
	{
		struct LowEntryJsonLibrary_eventObject_ToJsonString_Parms
		{
			ULowEntryJsonObject* JsonObject;
			bool PrettyPrint;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrettyPrint_MetaData[];
#endif
		static void NewProp_PrettyPrint_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_PrettyPrint;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_ToJsonString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_PrettyPrint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_PrettyPrint_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObject_ToJsonString_Parms*)Obj)->PrettyPrint = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_PrettyPrint = { UE4CodeGen_Private::EPropertyClass::Bool, "PrettyPrint", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObject_ToJsonString_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_PrettyPrint_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_PrettyPrint_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_PrettyPrint_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_ToJsonString_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_PrettyPrint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Json String|Stringify" },
		{ "CPP_Default_PrettyPrint", "true" },
		{ "DisplayName", "To Json String (Json Object)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a Json String from the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_ToJsonString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_ToJsonString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics
	{
		struct LowEntryJsonLibrary_eventObject_Unset_Parms
		{
			ULowEntryJsonObject* JsonObject;
			FString Key;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Unset_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObject_Unset_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Field" },
		{ "DisplayName", "Unset" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "If there is an entry with the given key, it will be removed from this Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Object_Unset", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObject_Unset_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics
	{
		struct LowEntryJsonLibrary_eventObjectEntry_Pure_Create_Parms
		{
			FString Key;
			ULowEntryJsonValue* Value;
			ULowEntryJsonObjectEntry* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_Create_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_Create_Parms, Value), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_Create_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::NewProp_Key,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create Entry (Pure)" },
		{ "DisplayName", "Create Json Object Entry (Json Value)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Entry." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectEntry_Pure_Create", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_Create_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromBoolean_Parms
		{
			FString Key;
			bool Value;
			ULowEntryJsonObjectEntry* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromBoolean_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromBoolean_Parms*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromBoolean_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::NewProp_Key,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create Entry (Pure)" },
		{ "DisplayName", "Create Json Object Entry (Boolean)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Entry and sets its value to the given Boolean." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectEntry_Pure_CreateFromBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics
	{
		struct LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromByte_Parms
		{
			FString Key;
			uint8 Value;
			ULowEntryJsonObjectEntry* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromByte_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromByte_Parms, Value), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromByte_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::NewProp_Key,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create Entry (Pure)" },
		{ "DisplayName", "Create Json Object Entry (Byte)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Entry and sets its value to the given Byte." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectEntry_Pure_CreateFromByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics
	{
		struct LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromFloat_Parms
		{
			FString Key;
			float Value;
			ULowEntryJsonObjectEntry* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromFloat_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromFloat_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromFloat_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::NewProp_Key,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create Entry (Pure)" },
		{ "DisplayName", "Create Json Object Entry (Float)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Entry and sets its value to the given Float." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectEntry_Pure_CreateFromFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics
	{
		struct LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromInteger_Parms
		{
			FString Key;
			int32 Value;
			ULowEntryJsonObjectEntry* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromInteger_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromInteger_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromInteger_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::NewProp_Key,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create Entry (Pure)" },
		{ "DisplayName", "Create Json Object Entry (Integer)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Entry and sets its value to the given Integer." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectEntry_Pure_CreateFromInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonArray_Parms
		{
			FString Key;
			ULowEntryJsonArray* Value;
			ULowEntryJsonObjectEntry* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonArray_Parms, Value), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonArray_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::NewProp_Key,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create Entry (Pure)" },
		{ "DisplayName", "Create Json Object Entry (Json Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Entry and sets its value to a copy the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectEntry_Pure_CreateFromJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonObject_Parms
		{
			FString Key;
			ULowEntryJsonObject* Value;
			ULowEntryJsonObjectEntry* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonObject_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonObject_Parms, Value), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonObject_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::NewProp_Key,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create Entry (Pure)" },
		{ "DisplayName", "Create Json Object Entry (Json Object)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Entry and sets its value to a copy the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectEntry_Pure_CreateFromJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics
	{
		struct LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromNull_Parms
		{
			FString Key;
			ULowEntryJsonObjectEntry* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromNull_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromNull_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::NewProp_Key,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create Entry (Pure)" },
		{ "DisplayName", "Create Json Object Entry (Null)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Entry and sets its value to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectEntry_Pure_CreateFromNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics
	{
		struct LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromString_Parms
		{
			FString Key;
			FString Value;
			ULowEntryJsonObjectEntry* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromString_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromString_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromString_Parms, Key), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::NewProp_Key,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Create Entry (Pure)" },
		{ "DisplayName", "Create Json Object Entry (String)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Entry and sets its value to the given String." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectEntry_Pure_CreateFromString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectEntry_Pure_CreateFromString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics
	{
		struct LowEntryJsonLibrary_eventObjectIterator_Clone_Parms
		{
			ULowEntryJsonObjectIterator* JsonObjectIterator;
			ULowEntryJsonObjectIterator* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObjectIterator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Clone_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectIterator_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::NewProp_JsonObjectIterator = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObjectIterator", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Clone_Parms, JsonObjectIterator), Z_Construct_UClass_ULowEntryJsonObjectIterator_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::NewProp_JsonObjectIterator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Iterator" },
		{ "DisplayName", "Clone Json Object Iterator" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Iterator that will iterate over the same keys and values as the given Json Object Iterator." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectIterator_Clone", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObjectIterator_Clone_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics
	{
		struct LowEntryJsonLibrary_eventObjectIterator_Create_Parms
		{
			ULowEntryJsonObject* JsonObject;
			ULowEntryJsonObjectIterator* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Create_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObjectIterator_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::NewProp_JsonObject = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObject", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Create_Parms, JsonObject), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::NewProp_JsonObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Iterator" },
		{ "DisplayName", "Create Json Object Iterator" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Object Iterator that will iterate over the keys and values of the given Json Object.\n\nThe Json Object Iterator will only iterate over the keys and values present in the Json Object at the time of creating this Json Object Iterator.\nIt will always keep iterating over the same keys and values, even when the Json Object has been changed, deleted, cleared, etc." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectIterator_Create", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObjectIterator_Create_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics
	{
		struct LowEntryJsonLibrary_eventObjectIterator_Get_Parms
		{
			ULowEntryJsonObjectIterator* JsonObjectIterator;
			int32 Index;
			FString Key;
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonValueFound Branch;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObjectIterator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Get_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonValueFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Get_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Get_Parms, Key), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Index = { UE4CodeGen_Private::EPropertyClass::Int, "Index", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Get_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Index_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_JsonObjectIterator = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObjectIterator", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Get_Parms, JsonObjectIterator), Z_Construct_UClass_ULowEntryJsonObjectIterator_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_JsonValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::NewProp_JsonObjectIterator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Iterator" },
		{ "DisplayName", "Get" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "If there is an entry with the given index, it will create and return a Json Value with its value set to a copy of the value of the entry found, it will also return the Json Object key it was assigned to during the creation of this Json Object Iterator." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectIterator_Get", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventObjectIterator_Get_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics
	{
		struct LowEntryJsonLibrary_eventObjectIterator_GetKeys_Parms
		{
			ULowEntryJsonObjectIterator* JsonObjectIterator;
			TArray<FString> ReturnValue;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObjectIterator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Array, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_GetKeys_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::NewProp_ReturnValue_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::NewProp_JsonObjectIterator = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObjectIterator", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_GetKeys_Parms, JsonObjectIterator), Z_Construct_UClass_ULowEntryJsonObjectIterator_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::NewProp_JsonObjectIterator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Iterator" },
		{ "DisplayName", "Get Keys" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the String Array the given Json Object Iterator iterates over." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectIterator_GetKeys", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObjectIterator_GetKeys_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics
	{
		struct LowEntryJsonLibrary_eventObjectIterator_GetValues_Parms
		{
			ULowEntryJsonObjectIterator* JsonObjectIterator;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObjectIterator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_GetValues_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::NewProp_JsonObjectIterator = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObjectIterator", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_GetValues_Parms, JsonObjectIterator), Z_Construct_UClass_ULowEntryJsonObjectIterator_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::NewProp_JsonObjectIterator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Iterator" },
		{ "DisplayName", "Get Values" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the Json Array the given Json Object Iterator iterates over." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectIterator_GetValues", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventObjectIterator_GetValues_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics
	{
		struct LowEntryJsonLibrary_eventObjectIterator_Length_Parms
		{
			ULowEntryJsonObjectIterator* JsonObjectIterator;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonObjectIterator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Length_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::NewProp_JsonObjectIterator = { UE4CodeGen_Private::EPropertyClass::Object, "JsonObjectIterator", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventObjectIterator_Length_Parms, JsonObjectIterator), Z_Construct_UClass_ULowEntryJsonObjectIterator_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::NewProp_JsonObjectIterator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Object|Iterator" },
		{ "DisplayName", "Length" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the number of entries the given Json Object Iterator iterates over." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ObjectIterator_Length", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventObjectIterator_Length_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics
	{
		struct LowEntryJsonLibrary_eventReadEveryFiles_Parms
		{
			TArray<FString> ReturnValue;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Array, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventReadEveryFiles_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::NewProp_ReturnValue_Inner = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::NewProp_ReturnValue_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::Function_MetaDataParams[] = {
		{ "Category", "Path" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ReadEveryFiles", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventReadEveryFiles_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics
	{
		struct LowEntryJsonLibrary_eventReadFile2_Parms
		{
			FString filename;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_filename;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventReadFile2_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::NewProp_filename = { UE4CodeGen_Private::EPropertyClass::Str, "filename", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventReadFile2_Parms, filename), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::NewProp_filename,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::Function_MetaDataParams[] = {
		{ "Category", "Path" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "ReadFile2", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventReadFile2_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Clone_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Clone_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Clone_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Clone" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to a copy of the value of the given Json Value." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Clone", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_Clone_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Create_Parms
		{
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Create_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Create Json Value" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Create", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_Create_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventValue_CreateFromBoolean_Parms
		{
			bool Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromBoolean_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventValue_CreateFromBoolean_Parms*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventValue_CreateFromBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Create Json Value (Boolean)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given Boolean." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_CreateFromBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_CreateFromBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics
	{
		struct LowEntryJsonLibrary_eventValue_CreateFromByte_Parms
		{
			uint8 Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromByte_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromByte_Parms, Value), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Create Json Value (Byte)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given Byte." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_CreateFromByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_CreateFromByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics
	{
		struct LowEntryJsonLibrary_eventValue_CreateFromFloat_Parms
		{
			float Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromFloat_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromFloat_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Create Json Value (Float)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given Float." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_CreateFromFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_CreateFromFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics
	{
		struct LowEntryJsonLibrary_eventValue_CreateFromInteger_Parms
		{
			int32 Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromInteger_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromInteger_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Create Json Value (Integer)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given Integer." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_CreateFromInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_CreateFromInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventValue_CreateFromJsonArray_Parms
		{
			ULowEntryJsonArray* Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromJsonArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromJsonArray_Parms, Value), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Create Json Value (Json Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to a copy the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_CreateFromJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_CreateFromJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventValue_CreateFromJsonObject_Parms
		{
			ULowEntryJsonObject* Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromJsonObject_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromJsonObject_Parms, Value), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Create Json Value (Json Object)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to a copy the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_CreateFromJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_CreateFromJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics
	{
		struct LowEntryJsonLibrary_eventValue_CreateFromNull_Parms
		{
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromNull_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Create Json Value (Null)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_CreateFromNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_CreateFromNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics
	{
		struct LowEntryJsonLibrary_eventValue_CreateFromString_Parms
		{
			FString Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromString_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_CreateFromString_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Callable)" },
		{ "DisplayName", "Create Json Value (String)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given String." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_CreateFromString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_CreateFromString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Get_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonType Branch;
			FString ReturnStringValue;
			int32 ReturnIntegerValue;
			float ReturnFloatValue;
			uint8 ReturnByteValue;
			bool ReturnBooleanValue;
			ULowEntryJsonObject* ReturnJsonObjectValue;
			ULowEntryJsonArray* ReturnJsonArrayValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnJsonArrayValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnJsonObjectValue;
		static void NewProp_ReturnBooleanValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnBooleanValue;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnByteValue;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnFloatValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnIntegerValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnStringValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnJsonArrayValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnJsonArrayValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Get_Parms, ReturnJsonArrayValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnJsonObjectValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnJsonObjectValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Get_Parms, ReturnJsonObjectValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnBooleanValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventValue_Get_Parms*)Obj)->ReturnBooleanValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnBooleanValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnBooleanValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventValue_Get_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnBooleanValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnByteValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnByteValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Get_Parms, ReturnByteValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnFloatValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnFloatValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Get_Parms, ReturnFloatValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnIntegerValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnIntegerValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Get_Parms, ReturnIntegerValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnStringValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnStringValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Get_Parms, ReturnStringValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Get_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Get_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnJsonArrayValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnJsonObjectValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnBooleanValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnByteValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnFloatValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnIntegerValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_ReturnStringValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value and the type of the value." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Get", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventValue_Get_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetAsBooleanOrDefault_Parms
		{
			ULowEntryJsonValue* JsonValue;
			bool Default;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static void NewProp_Default_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Default;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventValue_GetAsBooleanOrDefault_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventValue_GetAsBooleanOrDefault_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_Default_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventValue_GetAsBooleanOrDefault_Parms*)Obj)->Default = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Bool, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventValue_GetAsBooleanOrDefault_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_Default_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_Default_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsBooleanOrDefault_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get As Boolean" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Boolean, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetAsBooleanOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_GetAsBooleanOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetAsByteOrDefault_Parms
		{
			ULowEntryJsonValue* JsonValue;
			uint8 Default;
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Default;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsByteOrDefault_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Byte, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsByteOrDefault_Parms, Default), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::NewProp_Default_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsByteOrDefault_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get As Byte" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Byte, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetAsByteOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_GetAsByteOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetAsFloatOrDefault_Parms
		{
			ULowEntryJsonValue* JsonValue;
			float Default;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Default;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsFloatOrDefault_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Float, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsFloatOrDefault_Parms, Default), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::NewProp_Default_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsFloatOrDefault_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get As Float" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Float, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetAsFloatOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_GetAsFloatOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetAsIntegerOrDefault_Parms
		{
			ULowEntryJsonValue* JsonValue;
			int32 Default;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Default;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsIntegerOrDefault_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Int, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsIntegerOrDefault_Parms, Default), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::NewProp_Default_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsIntegerOrDefault_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get As Integer" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as an Integer, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetAsIntegerOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_GetAsIntegerOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetAsJsonArrayOrDefault_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ULowEntryJsonArray* Default;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Default;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsJsonArrayOrDefault_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Object, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsJsonArrayOrDefault_Parms, Default), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsJsonArrayOrDefault_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get As Json Array" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Array, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetAsJsonArrayOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_GetAsJsonArrayOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetAsJsonObjectOrDefault_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ULowEntryJsonObject* Default;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Default;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsJsonObjectOrDefault_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Object, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsJsonObjectOrDefault_Parms, Default), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsJsonObjectOrDefault_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get As Json Object" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Object, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetAsJsonObjectOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_GetAsJsonObjectOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetAsStringOrDefault_Parms
		{
			ULowEntryJsonValue* JsonValue;
			FString Default;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Default_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Default;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsStringOrDefault_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::NewProp_Default_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::NewProp_Default = { UE4CodeGen_Private::EPropertyClass::Str, "Default", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsStringOrDefault_Parms, Default), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::NewProp_Default_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::NewProp_Default_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetAsStringOrDefault_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::NewProp_Default,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get As String" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a String, returns the Default value if not possible." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetAsStringOrDefault", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_GetAsStringOrDefault_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetBoolean_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonTypeFound Branch;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventValue_GetBoolean_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventValue_GetBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetBoolean_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetBoolean_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get Boolean" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Boolean if the value is a Boolean." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventValue_GetBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetByte_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonTypeFound Branch;
			uint8 ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Byte, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetByte_Parms, ReturnValue), nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetByte_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetByte_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get Byte" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Byte if the value is a possible Byte (Integer between 0 and 255)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventValue_GetByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetFloat_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonTypeFound Branch;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Float, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetFloat_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetFloat_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetFloat_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get Float" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a Float if the value is a Float." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventValue_GetFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetInteger_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonTypeFound Branch;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Int, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetInteger_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetInteger_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetInteger_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get Integer" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as an Integer if the value is a possible Integer (Float with less than 0.001 away from a whole number)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventValue_GetInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetJsonArray_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonTypeFound Branch;
			ULowEntryJsonArray* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetJsonArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetJsonArray_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetJsonArray_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get Json Array" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Array if the value is a Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventValue_GetJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetJsonObject_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonTypeFound Branch;
			ULowEntryJsonObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetJsonObject_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetJsonObject_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetJsonObject_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get Json Object" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a copy of a Json Object if the value is a Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventValue_GetJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetNull_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonTypeFound Branch;
		};
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetNull_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetNull_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get Null" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Shows you if the value is null or not." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventValue_GetNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics
	{
		struct LowEntryJsonLibrary_eventValue_GetString_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ELowEntryJsonTypeFound Branch;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Branch;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Branch_Underlying;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Str, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::NewProp_Branch = { UE4CodeGen_Private::EPropertyClass::Enum, "Branch", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000180, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetString_Parms, Branch), Z_Construct_UEnum_LowEntryJson_ELowEntryJsonTypeFound, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::NewProp_Branch_Underlying = { UE4CodeGen_Private::EPropertyClass::Byte, "UnderlyingType", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_GetString_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::NewProp_Branch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::NewProp_Branch_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Get String" },
		{ "ExpandEnumAsExecs", "Branch" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Returns the value as a String if the value is a String." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_GetString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04422401, sizeof(LowEntryJsonLibrary_eventValue_GetString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_Clone_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_Clone_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_Clone_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Clone" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to a copy of the value of the given Json Value." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_Clone", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_Clone_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_Create_Parms
		{
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_Create_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Create Json Value" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_Create", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_Create_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_CreateFromBoolean_Parms
		{
			bool Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromBoolean_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventValue_Pure_CreateFromBoolean_Parms*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventValue_Pure_CreateFromBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Create Json Value (Boolean)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given Boolean." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_CreateFromBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_CreateFromBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_CreateFromByte_Parms
		{
			uint8 Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromByte_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromByte_Parms, Value), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Create Json Value (Byte)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given Byte." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_CreateFromByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_CreateFromByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_CreateFromFloat_Parms
		{
			float Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromFloat_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromFloat_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Create Json Value (Float)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given Float." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_CreateFromFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_CreateFromFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_CreateFromInteger_Parms
		{
			int32 Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromInteger_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromInteger_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Create Json Value (Integer)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given Integer." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_CreateFromInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_CreateFromInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_CreateFromJsonArray_Parms
		{
			ULowEntryJsonArray* Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromJsonArray_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromJsonArray_Parms, Value), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Create Json Value (Json Array)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to a copy the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_CreateFromJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_CreateFromJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_CreateFromJsonObject_Parms
		{
			ULowEntryJsonObject* Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromJsonObject_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromJsonObject_Parms, Value), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Create Json Value (Json Object)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to a copy the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_CreateFromJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_CreateFromJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_CreateFromNull_Parms
		{
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromNull_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Create Json Value (Null)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_CreateFromNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_CreateFromNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Pure_CreateFromString_Parms
		{
			FString Value;
			ULowEntryJsonValue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Object, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromString_Parms, ReturnValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Pure_CreateFromString_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Create (Pure)" },
		{ "DisplayName", "Create Json Value (String)" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Creates a new Json Value and sets its value to the given String." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Pure_CreateFromString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x14022401, sizeof(LowEntryJsonLibrary_eventValue_Pure_CreateFromString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics
	{
		struct LowEntryJsonLibrary_eventValue_Set_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ULowEntryJsonValue* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Set_Parms, Value), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_Set_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Set Json Value" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of this Json Value to a copy of the value of the given Json Value." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_Set", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_Set_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics
	{
		struct LowEntryJsonLibrary_eventValue_SetBoolean_Parms
		{
			ULowEntryJsonValue* JsonValue;
			bool Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static void NewProp_Value_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::NewProp_Value_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventValue_SetBoolean_Parms*)Obj)->Value = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Bool, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventValue_SetBoolean_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::NewProp_Value_SetBit, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetBoolean_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Set Boolean" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of this Json Value to the given Boolean." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_SetBoolean", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_SetBoolean_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics
	{
		struct LowEntryJsonLibrary_eventValue_SetByte_Parms
		{
			ULowEntryJsonValue* JsonValue;
			uint8 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Byte, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetByte_Parms, Value), nullptr, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetByte_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Set Byte" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of this Json Value to the given Byte." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_SetByte", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_SetByte_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics
	{
		struct LowEntryJsonLibrary_eventValue_SetFloat_Parms
		{
			ULowEntryJsonValue* JsonValue;
			float Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Float, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetFloat_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetFloat_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Set Float" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of this Json Value to the given Float." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_SetFloat", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_SetFloat_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics
	{
		struct LowEntryJsonLibrary_eventValue_SetInteger_Parms
		{
			ULowEntryJsonValue* JsonValue;
			int32 Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Int, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000082, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetInteger_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetInteger_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Set Integer" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of this Json Value to the given Integer." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_SetInteger", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_SetInteger_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics
	{
		struct LowEntryJsonLibrary_eventValue_SetJsonArray_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ULowEntryJsonArray* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetJsonArray_Parms, Value), Z_Construct_UClass_ULowEntryJsonArray_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetJsonArray_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Set Json Array" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of this Json Value to a copy of the given Json Array." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_SetJsonArray", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_SetJsonArray_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics
	{
		struct LowEntryJsonLibrary_eventValue_SetJsonObject_Parms
		{
			ULowEntryJsonValue* JsonValue;
			ULowEntryJsonObject* Value;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetJsonObject_Parms, Value), Z_Construct_UClass_ULowEntryJsonObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetJsonObject_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Set Json Object" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of this Json Value to a copy of the given Json Object." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_SetJsonObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_SetJsonObject_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics
	{
		struct LowEntryJsonLibrary_eventValue_SetNull_Parms
		{
			ULowEntryJsonValue* JsonValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetNull_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Set Null" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of this Json Value to null." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_SetNull", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_SetNull_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics
	{
		struct LowEntryJsonLibrary_eventValue_SetString_Parms
		{
			ULowEntryJsonValue* JsonValue;
			FString Value;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_JsonValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::NewProp_Value_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Str, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetString_Parms, Value), METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::NewProp_JsonValue = { UE4CodeGen_Private::EPropertyClass::Object, "JsonValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventValue_SetString_Parms, JsonValue), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::NewProp_JsonValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Low Entry|Json|Value|Value" },
		{ "DisplayName", "Set String" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
		{ "ToolTip", "Sets the value of this Json Value to the given String." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "Value_SetString", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventValue_SetString_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics
	{
		struct LowEntryJsonLibrary_eventWrriteFile_Parms
		{
			FString filename;
			FString WrriteData;
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_WrriteData;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_filename;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LowEntryJsonLibrary_eventWrriteFile_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::NewProp_ReturnValue = { UE4CodeGen_Private::EPropertyClass::Bool, "ReturnValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000580, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(LowEntryJsonLibrary_eventWrriteFile_Parms), &Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::NewProp_WrriteData = { UE4CodeGen_Private::EPropertyClass::Str, "WrriteData", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventWrriteFile_Parms, WrriteData), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::NewProp_filename = { UE4CodeGen_Private::EPropertyClass::Str, "filename", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(LowEntryJsonLibrary_eventWrriteFile_Parms, filename), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::NewProp_ReturnValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::NewProp_WrriteData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::NewProp_filename,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "Path" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULowEntryJsonLibrary, "WrriteFile", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04022401, sizeof(LowEntryJsonLibrary_eventWrriteFile_Parms), Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULowEntryJsonLibrary_NoRegister()
	{
		return ULowEntryJsonLibrary::StaticClass();
	}
	struct Z_Construct_UClass_ULowEntryJsonLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULowEntryJsonLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LowEntryJson,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULowEntryJsonLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddBoolean, "Array_AddBoolean" }, // 2513883403
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddByte, "Array_AddByte" }, // 3938545682
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddFloat, "Array_AddFloat" }, // 3882939916
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddInteger, "Array_AddInteger" }, // 521218402
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonArray, "Array_AddJsonArray" }, // 798426554
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonObject, "Array_AddJsonObject" }, // 2478722442
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddJsonValue, "Array_AddJsonValue" }, // 3069596621
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddNull, "Array_AddNull" }, // 2778883193
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_AddString, "Array_AddString" }, // 650111732
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Append, "Array_Append" }, // 4097575888
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clear, "Array_Clear" }, // 739781740
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Clone, "Array_Clone" }, // 2318565522
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromBooleanArray, "Array_CreateFromBooleanArray" }, // 363639141
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromByteArray, "Array_CreateFromByteArray" }, // 699454057
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromFloatArray, "Array_CreateFromFloatArray" }, // 2901177895
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromIntegerArray, "Array_CreateFromIntegerArray" }, // 559256285
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonArrayArray, "Array_CreateFromJsonArrayArray" }, // 3750349213
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonObjectArray, "Array_CreateFromJsonObjectArray" }, // 3517573342
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromJsonValueArray, "Array_CreateFromJsonValueArray" }, // 740365793
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_CreateFromStringArray, "Array_CreateFromStringArray" }, // 1568549422
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Expand, "Array_Expand" }, // 441284737
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Get, "Array_Get" }, // 654462785
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsBooleanOrDefault, "Array_GetAsBooleanOrDefault" }, // 624990627
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsByteOrDefault, "Array_GetAsByteOrDefault" }, // 2330128767
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsFloatOrDefault, "Array_GetAsFloatOrDefault" }, // 329049963
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsIntegerOrDefault, "Array_GetAsIntegerOrDefault" }, // 1857451312
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonArrayOrDefault, "Array_GetAsJsonArrayOrDefault" }, // 353508340
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonObjectOrDefault, "Array_GetAsJsonObjectOrDefault" }, // 2512738113
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsJsonValueOrDefault, "Array_GetAsJsonValueOrDefault" }, // 3846007746
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetAsStringOrDefault, "Array_GetAsStringOrDefault" }, // 3482045851
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetBoolean, "Array_GetBoolean" }, // 2887566272
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetByte, "Array_GetByte" }, // 2919621140
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetFloat, "Array_GetFloat" }, // 3198316949
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetInteger, "Array_GetInteger" }, // 2795430398
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonArray, "Array_GetJsonArray" }, // 2489799636
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonObject, "Array_GetJsonObject" }, // 1158297724
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetJsonValue, "Array_GetJsonValue" }, // 1326960958
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetNull, "Array_GetNull" }, // 2886370324
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_GetString, "Array_GetString" }, // 2655193391
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertBoolean, "Array_InsertBoolean" }, // 2801023587
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertByte, "Array_InsertByte" }, // 4081180706
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertFloat, "Array_InsertFloat" }, // 4253478882
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertInteger, "Array_InsertInteger" }, // 3616567244
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonArray, "Array_InsertJsonArray" }, // 4284948824
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonObject, "Array_InsertJsonObject" }, // 1667007761
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertJsonValue, "Array_InsertJsonValue" }, // 1720836874
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertNull, "Array_InsertNull" }, // 3726134060
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_InsertString, "Array_InsertString" }, // 1014869388
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_IsSet, "Array_IsSet" }, // 1757975834
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Length, "Array_Length" }, // 4243213551
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Merge, "Array_Merge" }, // 3986814200
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Clone, "Array_Pure_Clone" }, // 2779435898
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromBooleanArray, "Array_Pure_CreateFromBooleanArray" }, // 940295785
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromByteArray, "Array_Pure_CreateFromByteArray" }, // 1982641864
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromFloatArray, "Array_Pure_CreateFromFloatArray" }, // 3585972730
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromIntegerArray, "Array_Pure_CreateFromIntegerArray" }, // 4045375958
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonArrayArray, "Array_Pure_CreateFromJsonArrayArray" }, // 3622150969
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonObjectArray, "Array_Pure_CreateFromJsonObjectArray" }, // 4240040500
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromJsonValueArray, "Array_Pure_CreateFromJsonValueArray" }, // 2436300961
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_CreateFromStringArray, "Array_Pure_CreateFromStringArray" }, // 1512146363
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_Merge, "Array_Pure_Merge" }, // 508199149
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Pure_ToJsonString, "Array_Pure_ToJsonString" }, // 1107131297
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Resize, "Array_Resize" }, // 898187346
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetBoolean, "Array_SetBoolean" }, // 380558048
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetByte, "Array_SetByte" }, // 3475674964
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetFloat, "Array_SetFloat" }, // 1667159126
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetInteger, "Array_SetInteger" }, // 1192658203
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonArray, "Array_SetJsonArray" }, // 2678941165
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonObject, "Array_SetJsonObject" }, // 3690245463
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetJsonValue, "Array_SetJsonValue" }, // 3069754483
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetNull, "Array_SetNull" }, // 3615965281
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_SetString, "Array_SetString" }, // 2065312894
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Shorten, "Array_Shorten" }, // 1027298923
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_ToJsonString, "Array_ToJsonString" }, // 2622646429
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Array_Unset, "Array_Unset" }, // 3370222939
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Json_ParseJsonString, "Json_ParseJsonString" }, // 2495631446
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Append, "Object_Append" }, // 3631838925
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clear, "Object_Clear" }, // 4223849826
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Clone, "Object_Clone" }, // 1963308890
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_CreateFromObjectEntryArray, "Object_CreateFromObjectEntryArray" }, // 1335483780
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Get, "Object_Get" }, // 3237246463
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsBooleanOrDefault, "Object_GetAsBooleanOrDefault" }, // 2490086222
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsByteOrDefault, "Object_GetAsByteOrDefault" }, // 1589872755
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsFloatOrDefault, "Object_GetAsFloatOrDefault" }, // 2669537036
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsIntegerOrDefault, "Object_GetAsIntegerOrDefault" }, // 162668755
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonArrayOrDefault, "Object_GetAsJsonArrayOrDefault" }, // 3849529119
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonObjectOrDefault, "Object_GetAsJsonObjectOrDefault" }, // 182504008
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsJsonValueOrDefault, "Object_GetAsJsonValueOrDefault" }, // 4225026569
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetAsStringOrDefault, "Object_GetAsStringOrDefault" }, // 1502338232
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetBoolean, "Object_GetBoolean" }, // 358452632
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetByte, "Object_GetByte" }, // 2773292296
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetFloat, "Object_GetFloat" }, // 3015304962
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetInteger, "Object_GetInteger" }, // 4137403715
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonArray, "Object_GetJsonArray" }, // 1573359774
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonObject, "Object_GetJsonObject" }, // 3753468114
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetJsonValue, "Object_GetJsonValue" }, // 2218753575
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetKeys, "Object_GetKeys" }, // 2088251207
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetNull, "Object_GetNull" }, // 540522006
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetString, "Object_GetString" }, // 2904148556
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_GetValues, "Object_GetValues" }, // 1786060941
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_IsSet, "Object_IsSet" }, // 1310131687
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Length, "Object_Length" }, // 1626972549
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Merge, "Object_Merge" }, // 752705590
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Clone, "Object_Pure_Clone" }, // 1000542251
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_CreateFromObjectEntryArray, "Object_Pure_CreateFromObjectEntryArray" }, // 2525250538
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_Merge, "Object_Pure_Merge" }, // 2396818654
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Pure_ToJsonString, "Object_Pure_ToJsonString" }, // 2548845955
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetBoolean, "Object_SetBoolean" }, // 2936785404
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetByte, "Object_SetByte" }, // 1487366151
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetFloat, "Object_SetFloat" }, // 746980548
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetInteger, "Object_SetInteger" }, // 4212012839
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonArray, "Object_SetJsonArray" }, // 2392540922
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonObject, "Object_SetJsonObject" }, // 1901197822
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetJsonValue, "Object_SetJsonValue" }, // 3706671549
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetNull, "Object_SetNull" }, // 125526944
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_SetString, "Object_SetString" }, // 662254618
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Sort, "Object_Sort" }, // 2214200986
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_ToJsonString, "Object_ToJsonString" }, // 3534958091
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Object_Unset, "Object_Unset" }, // 2530206539
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_Create, "ObjectEntry_Pure_Create" }, // 1988236790
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromBoolean, "ObjectEntry_Pure_CreateFromBoolean" }, // 3752812189
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromByte, "ObjectEntry_Pure_CreateFromByte" }, // 1639997705
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromFloat, "ObjectEntry_Pure_CreateFromFloat" }, // 1588258621
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromInteger, "ObjectEntry_Pure_CreateFromInteger" }, // 3397094118
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonArray, "ObjectEntry_Pure_CreateFromJsonArray" }, // 1051690043
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromJsonObject, "ObjectEntry_Pure_CreateFromJsonObject" }, // 4021810612
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromNull, "ObjectEntry_Pure_CreateFromNull" }, // 1490495587
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectEntry_Pure_CreateFromString, "ObjectEntry_Pure_CreateFromString" }, // 1983990377
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Clone, "ObjectIterator_Clone" }, // 3189061457
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Create, "ObjectIterator_Create" }, // 2535745248
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Get, "ObjectIterator_Get" }, // 1970342394
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetKeys, "ObjectIterator_GetKeys" }, // 3928268570
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_GetValues, "ObjectIterator_GetValues" }, // 1654220700
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ObjectIterator_Length, "ObjectIterator_Length" }, // 1074829560
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ReadEveryFiles, "ReadEveryFiles" }, // 2626768984
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_ReadFile2, "ReadFile2" }, // 2455093972
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Clone, "Value_Clone" }, // 370269887
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Create, "Value_Create" }, // 2546032610
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromBoolean, "Value_CreateFromBoolean" }, // 3541302764
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromByte, "Value_CreateFromByte" }, // 2326843505
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromFloat, "Value_CreateFromFloat" }, // 4262409732
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromInteger, "Value_CreateFromInteger" }, // 1052136813
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonArray, "Value_CreateFromJsonArray" }, // 383424994
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromJsonObject, "Value_CreateFromJsonObject" }, // 457001146
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromNull, "Value_CreateFromNull" }, // 3294788129
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_CreateFromString, "Value_CreateFromString" }, // 1651185525
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Get, "Value_Get" }, // 1352362865
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsBooleanOrDefault, "Value_GetAsBooleanOrDefault" }, // 119957445
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsByteOrDefault, "Value_GetAsByteOrDefault" }, // 1058810833
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsFloatOrDefault, "Value_GetAsFloatOrDefault" }, // 4831148
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsIntegerOrDefault, "Value_GetAsIntegerOrDefault" }, // 1875763637
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonArrayOrDefault, "Value_GetAsJsonArrayOrDefault" }, // 2781042767
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsJsonObjectOrDefault, "Value_GetAsJsonObjectOrDefault" }, // 320482906
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetAsStringOrDefault, "Value_GetAsStringOrDefault" }, // 1780943922
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetBoolean, "Value_GetBoolean" }, // 2698094159
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetByte, "Value_GetByte" }, // 1288836202
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetFloat, "Value_GetFloat" }, // 2783912033
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetInteger, "Value_GetInteger" }, // 3620655945
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonArray, "Value_GetJsonArray" }, // 4051485364
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetJsonObject, "Value_GetJsonObject" }, // 3001722266
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetNull, "Value_GetNull" }, // 2618686085
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_GetString, "Value_GetString" }, // 2450453411
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Clone, "Value_Pure_Clone" }, // 217530595
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_Create, "Value_Pure_Create" }, // 1826067938
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromBoolean, "Value_Pure_CreateFromBoolean" }, // 87148389
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromByte, "Value_Pure_CreateFromByte" }, // 3014425713
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromFloat, "Value_Pure_CreateFromFloat" }, // 1694408047
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromInteger, "Value_Pure_CreateFromInteger" }, // 2261690598
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonArray, "Value_Pure_CreateFromJsonArray" }, // 3526662637
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromJsonObject, "Value_Pure_CreateFromJsonObject" }, // 447723182
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromNull, "Value_Pure_CreateFromNull" }, // 3926765573
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Pure_CreateFromString, "Value_Pure_CreateFromString" }, // 2674228828
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_Set, "Value_Set" }, // 323242375
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetBoolean, "Value_SetBoolean" }, // 152916271
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetByte, "Value_SetByte" }, // 1533599152
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetFloat, "Value_SetFloat" }, // 2029143676
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetInteger, "Value_SetInteger" }, // 3190026463
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonArray, "Value_SetJsonArray" }, // 3451202357
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetJsonObject, "Value_SetJsonObject" }, // 1940515744
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetNull, "Value_SetNull" }, // 3063022212
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_Value_SetString, "Value_SetString" }, // 1698641087
		{ &Z_Construct_UFunction_ULowEntryJsonLibrary_WrriteFile, "WrriteFile" }, // 3994806020
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULowEntryJsonLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Classes/LowEntryJsonLibrary.h" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULowEntryJsonLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULowEntryJsonLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULowEntryJsonLibrary_Statics::ClassParams = {
		&ULowEntryJsonLibrary::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		nullptr, 0,
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ULowEntryJsonLibrary_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ULowEntryJsonLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULowEntryJsonLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULowEntryJsonLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULowEntryJsonLibrary, 3552384887);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULowEntryJsonLibrary(Z_Construct_UClass_ULowEntryJsonLibrary, &ULowEntryJsonLibrary::StaticClass, TEXT("/Script/LowEntryJson"), TEXT("ULowEntryJsonLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULowEntryJsonLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
