// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOWENTRYJSON_LowEntryJsonObject_generated_h
#error "LowEntryJsonObject.generated.h already included, missing '#pragma once' in LowEntryJsonObject.h"
#endif
#define LOWENTRYJSON_LowEntryJsonObject_generated_h

#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_RPC_WRAPPERS
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULowEntryJsonObject(); \
	friend struct Z_Construct_UClass_ULowEntryJsonObject_Statics; \
public: \
	DECLARE_CLASS(ULowEntryJsonObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonObject)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_INCLASS \
private: \
	static void StaticRegisterNativesULowEntryJsonObject(); \
	friend struct Z_Construct_UClass_ULowEntryJsonObject_Statics; \
public: \
	DECLARE_CLASS(ULowEntryJsonObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonObject)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonObject(ULowEntryJsonObject&&); \
	NO_API ULowEntryJsonObject(const ULowEntryJsonObject&); \
public:


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonObject(ULowEntryJsonObject&&); \
	NO_API ULowEntryJsonObject(const ULowEntryJsonObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonObject)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_PRIVATE_PROPERTY_OFFSET
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_24_PROLOG
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_RPC_WRAPPERS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_INCLASS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_INCLASS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h_27_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LowEntryJsonObject."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
