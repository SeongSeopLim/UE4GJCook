// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
struct FLinearColor;
class ULowEntryJsonValue;
class ULowEntryJsonArray;
class ULowEntryJsonObject;
#ifdef LOWENTRYJSON_LowEntryJsonDebugLibrary_generated_h
#error "LowEntryJsonDebugLibrary.generated.h already included, missing '#pragma once' in LowEntryJsonDebugLibrary.h"
#endif
#define LOWENTRYJSON_LowEntryJsonDebugLibrary_generated_h

#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPrintBooleanArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(bool,Z_Param_Out_BooleanArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintBooleanArray(Z_Param_WorldContextObject,Z_Param_Out_BooleanArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintByteArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_ByteArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintByteArray(Z_Param_WorldContextObject,Z_Param_Out_ByteArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintFloatArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(float,Z_Param_Out_FloatArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintFloatArray(Z_Param_WorldContextObject,Z_Param_Out_FloatArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintIntegerArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(int32,Z_Param_Out_IntegerArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintIntegerArray(Z_Param_WorldContextObject,Z_Param_Out_IntegerArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintStringArrayEscaped) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(FString,Z_Param_Out_StringArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintStringArrayEscaped(Z_Param_WorldContextObject,Z_Param_Out_StringArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonValueArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(ULowEntryJsonValue*,Z_Param_Out_JsonValueArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonValueArray(Z_Param_WorldContextObject,Z_Param_Out_JsonValueArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonArrayArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(ULowEntryJsonArray*,Z_Param_Out_JsonArrayArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonArrayArray(Z_Param_WorldContextObject,Z_Param_Out_JsonArrayArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonObjectArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(ULowEntryJsonObject*,Z_Param_Out_JsonObjectArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonObjectArray(Z_Param_WorldContextObject,Z_Param_Out_JsonObjectArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintBoolean) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_UBOOL(Z_Param_Boolean); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintBoolean(Z_Param_WorldContextObject,Z_Param_Boolean,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintByte) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Byte); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintByte(Z_Param_WorldContextObject,Z_Param_Byte,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintFloat) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Float); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintFloat(Z_Param_WorldContextObject,Z_Param_Float,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintInteger) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Integer); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintInteger(Z_Param_WorldContextObject,Z_Param_Integer,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintStringEscaped) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_String); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintStringEscaped(Z_Param_WorldContextObject,Z_Param_String,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonValue) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonValue(Z_Param_WorldContextObject,Z_Param_JsonValue,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonArray(Z_Param_WorldContextObject,Z_Param_JsonArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonObject) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonObject(Z_Param_WorldContextObject,Z_Param_JsonObject,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	}


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPrintBooleanArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(bool,Z_Param_Out_BooleanArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintBooleanArray(Z_Param_WorldContextObject,Z_Param_Out_BooleanArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintByteArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(uint8,Z_Param_Out_ByteArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintByteArray(Z_Param_WorldContextObject,Z_Param_Out_ByteArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintFloatArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(float,Z_Param_Out_FloatArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintFloatArray(Z_Param_WorldContextObject,Z_Param_Out_FloatArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintIntegerArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(int32,Z_Param_Out_IntegerArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintIntegerArray(Z_Param_WorldContextObject,Z_Param_Out_IntegerArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintStringArrayEscaped) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(FString,Z_Param_Out_StringArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintStringArrayEscaped(Z_Param_WorldContextObject,Z_Param_Out_StringArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonValueArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(ULowEntryJsonValue*,Z_Param_Out_JsonValueArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonValueArray(Z_Param_WorldContextObject,Z_Param_Out_JsonValueArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonArrayArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(ULowEntryJsonArray*,Z_Param_Out_JsonArrayArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonArrayArray(Z_Param_WorldContextObject,Z_Param_Out_JsonArrayArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonObjectArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_TARRAY_REF(ULowEntryJsonObject*,Z_Param_Out_JsonObjectArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonObjectArray(Z_Param_WorldContextObject,Z_Param_Out_JsonObjectArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintBoolean) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_UBOOL(Z_Param_Boolean); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintBoolean(Z_Param_WorldContextObject,Z_Param_Boolean,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintByte) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UByteProperty,Z_Param_Byte); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintByte(Z_Param_WorldContextObject,Z_Param_Byte,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintFloat) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Float); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintFloat(Z_Param_WorldContextObject,Z_Param_Float,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintInteger) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UIntProperty,Z_Param_Integer); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintInteger(Z_Param_WorldContextObject,Z_Param_Integer,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintStringEscaped) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_String); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintStringEscaped(Z_Param_WorldContextObject,Z_Param_String,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonValue) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(ULowEntryJsonValue,Z_Param_JsonValue); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonValue(Z_Param_WorldContextObject,Z_Param_JsonValue,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonArray) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(ULowEntryJsonArray,Z_Param_JsonArray); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonArray(Z_Param_WorldContextObject,Z_Param_JsonArray,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrintJsonObject) \
	{ \
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject); \
		P_GET_OBJECT(ULowEntryJsonObject,Z_Param_JsonObject); \
		P_GET_PROPERTY(UStrProperty,Z_Param_Label); \
		P_GET_PROPERTY(UFloatProperty,Z_Param_ScreenDurationTime); \
		P_GET_UBOOL(Z_Param_bPrintToScreen); \
		P_GET_UBOOL(Z_Param_bPrintToLog); \
		P_GET_STRUCT(FLinearColor,Z_Param_TextColor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		ULowEntryJsonDebugLibrary::PrintJsonObject(Z_Param_WorldContextObject,Z_Param_JsonObject,Z_Param_Label,Z_Param_ScreenDurationTime,Z_Param_bPrintToScreen,Z_Param_bPrintToLog,Z_Param_TextColor); \
		P_NATIVE_END; \
	}


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULowEntryJsonDebugLibrary(); \
	friend struct Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics; \
public: \
	DECLARE_CLASS(ULowEntryJsonDebugLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonDebugLibrary)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_INCLASS \
private: \
	static void StaticRegisterNativesULowEntryJsonDebugLibrary(); \
	friend struct Z_Construct_UClass_ULowEntryJsonDebugLibrary_Statics; \
public: \
	DECLARE_CLASS(ULowEntryJsonDebugLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJson"), NO_API) \
	DECLARE_SERIALIZER(ULowEntryJsonDebugLibrary)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonDebugLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonDebugLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonDebugLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonDebugLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonDebugLibrary(ULowEntryJsonDebugLibrary&&); \
	NO_API ULowEntryJsonDebugLibrary(const ULowEntryJsonDebugLibrary&); \
public:


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULowEntryJsonDebugLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULowEntryJsonDebugLibrary(ULowEntryJsonDebugLibrary&&); \
	NO_API ULowEntryJsonDebugLibrary(const ULowEntryJsonDebugLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULowEntryJsonDebugLibrary); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULowEntryJsonDebugLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULowEntryJsonDebugLibrary)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_PRIVATE_PROPERTY_OFFSET
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_22_PROLOG
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_RPC_WRAPPERS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_INCLASS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_INCLASS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h_25_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LowEntryJsonDebugLibrary."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PluginVersining_Plugins_LowEntryJson_Source_LowEntryJson_Public_Classes_LowEntryJsonDebugLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
