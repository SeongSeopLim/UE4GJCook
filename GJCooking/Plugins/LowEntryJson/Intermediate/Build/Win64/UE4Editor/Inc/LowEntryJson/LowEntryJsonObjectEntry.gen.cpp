// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LowEntryJson/Public/Classes/LowEntryJsonObjectEntry.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLowEntryJsonObjectEntry() {}
// Cross Module References
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonObjectEntry();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_LowEntryJson();
	LOWENTRYJSON_API UClass* Z_Construct_UClass_ULowEntryJsonValue_NoRegister();
// End Cross Module References
	void ULowEntryJsonObjectEntry::StaticRegisterNativesULowEntryJsonObjectEntry()
	{
	}
	UClass* Z_Construct_UClass_ULowEntryJsonObjectEntry_NoRegister()
	{
		return ULowEntryJsonObjectEntry::StaticClass();
	}
	struct Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LowEntryJson,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Classes/LowEntryJsonObjectEntry.h" },
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonObjectEntry.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Value_MetaData[] = {
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonObjectEntry.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Value = { UE4CodeGen_Private::EPropertyClass::Object, "Value", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000000, 1, nullptr, STRUCT_OFFSET(ULowEntryJsonObjectEntry, Value), Z_Construct_UClass_ULowEntryJsonValue_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Value_MetaData, ARRAY_COUNT(Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Key_MetaData[] = {
		{ "ModuleRelativePath", "Public/Classes/LowEntryJsonObjectEntry.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Key = { UE4CodeGen_Private::EPropertyClass::Str, "Key", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000000, 1, nullptr, STRUCT_OFFSET(ULowEntryJsonObjectEntry, Key), METADATA_PARAMS(Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Key_MetaData, ARRAY_COUNT(Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::NewProp_Key,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULowEntryJsonObjectEntry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::ClassParams = {
		&ULowEntryJsonObjectEntry::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x001000A0u,
		nullptr, 0,
		Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULowEntryJsonObjectEntry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULowEntryJsonObjectEntry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULowEntryJsonObjectEntry, 2570082826);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULowEntryJsonObjectEntry(Z_Construct_UClass_ULowEntryJsonObjectEntry, &ULowEntryJsonObjectEntry::StaticClass, TEXT("/Script/LowEntryJson"), TEXT("ULowEntryJsonObjectEntry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULowEntryJsonObjectEntry);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
