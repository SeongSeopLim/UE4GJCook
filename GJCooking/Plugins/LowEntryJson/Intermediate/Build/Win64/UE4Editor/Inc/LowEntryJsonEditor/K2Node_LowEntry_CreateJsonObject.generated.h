// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOWENTRYJSONEDITOR_K2Node_LowEntry_CreateJsonObject_generated_h
#error "K2Node_LowEntry_CreateJsonObject.generated.h already included, missing '#pragma once' in K2Node_LowEntry_CreateJsonObject.h"
#endif
#define LOWENTRYJSONEDITOR_K2Node_LowEntry_CreateJsonObject_generated_h

#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_RPC_WRAPPERS
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_LowEntry_CreateJsonObject(); \
	friend struct Z_Construct_UClass_UK2Node_LowEntry_CreateJsonObject_Statics; \
public: \
	DECLARE_CLASS(UK2Node_LowEntry_CreateJsonObject, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJsonEditor"), LOWENTRYJSONEDITOR_API) \
	DECLARE_SERIALIZER(UK2Node_LowEntry_CreateJsonObject)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_LowEntry_CreateJsonObject(); \
	friend struct Z_Construct_UClass_UK2Node_LowEntry_CreateJsonObject_Statics; \
public: \
	DECLARE_CLASS(UK2Node_LowEntry_CreateJsonObject, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LowEntryJsonEditor"), LOWENTRYJSONEDITOR_API) \
	DECLARE_SERIALIZER(UK2Node_LowEntry_CreateJsonObject)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_LowEntry_CreateJsonObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LOWENTRYJSONEDITOR_API, UK2Node_LowEntry_CreateJsonObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_LowEntry_CreateJsonObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonObject(UK2Node_LowEntry_CreateJsonObject&&); \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonObject(const UK2Node_LowEntry_CreateJsonObject&); \
public:


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonObject(UK2Node_LowEntry_CreateJsonObject&&); \
	LOWENTRYJSONEDITOR_API UK2Node_LowEntry_CreateJsonObject(const UK2Node_LowEntry_CreateJsonObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(LOWENTRYJSONEDITOR_API, UK2Node_LowEntry_CreateJsonObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_LowEntry_CreateJsonObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_LowEntry_CreateJsonObject)


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_PRIVATE_PROPERTY_OFFSET
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_19_PROLOG
#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_RPC_WRAPPERS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_INCLASS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_PRIVATE_PROPERTY_OFFSET \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_INCLASS_NO_PURE_DECLS \
	PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h_22_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class K2Node_LowEntry_CreateJsonObject."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PluginVersining_Plugins_LowEntryJson_Source_LowEntryJsonEditor_Public_Classes_K2Node_LowEntry_CreateJsonObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
