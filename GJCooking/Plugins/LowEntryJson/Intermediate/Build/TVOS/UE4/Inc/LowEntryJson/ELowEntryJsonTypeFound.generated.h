// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LOWENTRYJSON_ELowEntryJsonTypeFound_generated_h
#error "ELowEntryJsonTypeFound.generated.h already included, missing '#pragma once' in ELowEntryJsonTypeFound.h"
#endif
#define LOWENTRYJSON_ELowEntryJsonTypeFound_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID HostProject_Plugins_LowEntryJson_Source_LowEntryJson_Public_Enums_ELowEntryJsonTypeFound_h


#define FOREACH_ENUM_ELOWENTRYJSONTYPEFOUND(op) \
	op(ELowEntryJsonTypeFound::Success) \
	op(ELowEntryJsonTypeFound::WrongType) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
